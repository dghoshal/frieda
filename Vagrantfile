# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "Trusty64"

  # The url from where the 'config.vm.box' box will be fetched if it
  # doesn't already exist on the user's system.
  config.vm.box_url = "https://oss-binaries.phusionpassenger.com/vagrant/boxes/latest/ubuntu-14.04-amd64-vbox.box"


  config.vm.provider "virtualbox" do |v, override|
    v.memory = 2048
    v.cpus = 4
  end

   config.vm.provider :aws do |aws, override|
    override.vm.box = "dummy"
    override.vm.box_url = "https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box"

    aws.access_key_id = ENV['EC2_ACCESS_KEY']
    aws.secret_access_key = ENV['EC2_SECRET_KEY']
    aws.keypair_name = ENV['EC2_KEYPAIR']
    aws.instance_type = "t1.micro"
    aws.region = "us-west-1"
    aws.security_groups = "default"

    # HVM - Ubuntu Trusty 64
    #aws.ami = "ami-076e6542"

    # EBS - Ubuntu Trusty 64
    aws.ami = "ami-736e6536"

    override.ssh.username = "ubuntu"
    override.ssh.private_key_path = ENV['HOME']+"/.ssh/"+ENV['EC2_KEYPAIR']+".pem"
  end


  config.vm.provision "shell", inline: "sudo apt-get -y update", privileged: true
  config.vm.provision "shell", inline: "sudo apt-get -y install puppet", privileged: true

  config.vm.provider "vmware_fusion" do |v, override|
    override.vm.box = "Trusty64VMWare"
    override.vm.box_url = "https://oss-binaries.phusionpassenger.com/vagrant/boxes/latest/ubuntu-14.04-amd64-vmwarefusion.box"
    v.vmx["memsize"] = "2048"
    v.vmx["numvcpus"] = "4"
  end

  # Enable provisioning with Puppet stand alone.  Puppet manifests
  # are contained in a directory path relative to this Vagrantfile.
  # You will need to create the manifests directory and a manifest in
  # the file base.pp in the manifests_path directory.
  #
  # An example Puppet manifest to provision the message of the day:
  #
  # # group { "puppet":
  # #   ensure => "present",
  # # }
  # #
  # # File { owner => 0, group => 0, mode => 0644 }
  # #
  # # file { '/etc/motd':
  # #   content => "Welcome to your Vagrant-built virtual machine!
  # #               Managed by Puppet.\n"
  # # }
  #
  config.vm.provision :puppet do |puppet|
    puppet.manifests_path = "manifests"
    puppet.manifest_file  = "default.pp"
  end

end
