Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

package {['subversion','python-pip', 'python-setuptools', 'python-virtualenv','python-dev','pssh']:
  ensure=>installed,
  notify => File['/usr/local/bin/pssh',
                '/usr/local/bin/pscp',
                '/usr/local/bin/pnuke',
                '/usr/local/bin/pslurp',
                '/usr/local/bin/prsync']
}

file { '/usr/local/bin/pssh':
   ensure => 'link',
   target => '/usr/bin/parallel-ssh',
}

file { '/usr/local/bin/pscp':
   ensure => 'link',
   target => '/usr/bin/parallel-scp',
}

file { '/usr/local/bin/prsync':
   ensure => 'link',
   target => '/usr/bin/parallel-rsync',
}

file { '/usr/local/bin/pslurp':
   ensure => 'link',
   target => '/usr/bin/parallel-slurp',
}

file { '/usr/local/bin/pnuke':
   ensure => 'link',
   target => '/usr/bin/parallel-nuke',
}