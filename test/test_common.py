'''
Created on Dec 12, 2012

@author: val
'''
import unittest
import os
import shutil

import frieda.execute.common as common


class Test(unittest.TestCase):


    def setUp(self):
        self.testPath="%s/output" % os.getcwd()
        if not os.path.exists(self.testPath):
            os.mkdir(self.testPath)
        pass


    def tearDown(self):
        if os.path.exists(self.testPath):
            shutil.rmtree(self.testPath)
            pass
    
    def testOpenWithMode(self):
        
        testFile="%s/test.txt" % self.testPath
        f=common.openWithMode(testFile, 0660)
        
        fstat= os.fstat(f.fileno())
        
        assert( fstat.st_mode == 0100660)
        
    def testWhich(self):
        self.assertEqual(common.which("ls"), "/bin/ls") 
        
        
    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()