#!/usr/bin/python
import commands
import sys
import time
import os
from collections import namedtuple
import shutil
import logging
import frieda.util.integration
import pdb

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, filename="test_runner.log")
LOG = logging.getLogger(__name__)

Experiment = namedtuple('Experiment', 'app_name fs_type instance_type_name input_size cores')

VM_LOCAL_SIZES = [None, 0, 20, 40, 80, 160]
VM_CORES = [None, 1, 1, 2, 4, 8]
VM_NAMES = [None, 'tiny', 'small', 'medium', 'large', 'xlarge']


def main(args):
    LOG.info("\n*****************\nTest Runner Start\n****************\n")
    start = time.time()
    app_name = args[1]
    fs_types = args[2].split(',')
    instance_types = [int(i) for i in args[3].split(',')]
    input_sizes = [int(i) for i in args[4].split(',')]
    num_cores = int(args[5])

    for fs_type in fs_types:
        for instance_type in instance_types:
            LOG.info("Creating '%s' Experiments File System:%s Instance Type:%s" % (
                app_name, fs_type, VM_NAMES[instance_type]))

            num_vms = num_cores / VM_CORES[instance_type]
            experiments = []
            for s in input_sizes:
                input_size = int(s)
                experiment = Experiment(input_size=input_size, cores=num_vms * VM_CORES[instance_type],
                                        instance_type_name=VM_NAMES[instance_type], app_name=app_name, fs_type=fs_type)
                LOG.info(experiment)
                experiments.append(experiment)

            config = 'resources/configs/%(app_name)s/%(app_name)s-os-%(fs_type)s' % {'app_name': app_name,
                                                                                     'fs_type': fs_type}
            LOG.info("Staring Experiment '%s'  File System:%s Instance Type:%s" % (
                app_name, fs_type, VM_NAMES[instance_type]))
            test_suite(experiments, app_name, fs_type, instance_type, num_vms, config, data_dir="/data",
                       input_dir=app_name)
            time.sleep(60)
    end = time.time()
    LOG.info("TestRunner Time: %f" % (end - start))
    LOG.info("\n*****************\nTest Runner End\n****************\n")


def execute_integration(config_file, action, nodes=None):
    """
    Run contextualization for the given nodes

    :param config_file:  The configuration file to use
    :param action: The contextualization action
    :param nodes: The nodes to contextualize
    :type nodes: list
    :return:
    """
    LOG.info("\n*****************\nRun '%s' Start\n****************\n" % action)
    start = time.time()
    command_format = "python integration %(config)s %(action)s %(nodes)s"
    command_args = {'config': config_file, 'action': action, 'nodes': nodes}
    LOG.info(command_format % command_args)
    main_args = [config_file, action]
    if nodes:
        main_args.extend(nodes.split(" "))
    integration.main(main_args)
    end = time.time()
    LOG.info("Run '%s' Time: %f" % (action, (end - start)))
    LOG.info("\n*****************\nRun '%s' End\n****************\n" % action)


def run_experiment(experiment, config_file, output_dir, ssh_key):
    """
    Run an experiment for the given comfiguration file

    :param experiment: The experiment to run
    :param config_file:  The configuration file to use
    :param output_dir: The base output directory for the experiment results
    :param ssh_key: ssh key for vm connection
    """
    LOG.info("\n*****************\nRun App '%s' Start\n****************\n" % experiment.app_name)
    start = time.time()
    LOG.info("Starting the master")
    execute_integration(config_file, 'master', os.environ['MASTER_ID'])

    LOG.info("Starting the app")
    execute_integration(config_file, 'execute', os.environ['MASTER_ID'])
    LOG.info("Ending the app")

    LOG.info("Cleaning up the logs")
    execute_integration(config_file, 'clean-worker', os.environ['WORKER_IDS'])

    results_dir = '%(cwd)s/%(output_dir)s/%(app_name)s/%(fs_type)s/%(type)s/%(app_name)s-%(fs_type)s-%(type)s-%(cores)s-%(input_size)s' % {
        'cwd': os.getcwd(),
        'output_dir': output_dir,
        'app_name': experiment.app_name,
        'fs_type': experiment.fs_type,
        'type': experiment.instance_type_name,
        'cores': experiment.cores,
        'input_size': experiment.input_size}

    LOG.info(results_dir)
    if not os.path.exists(results_dir):
        os.makedirs(results_dir)
    else:
        try:
            shutil.rmtree(results_dir)
            os.makedirs(results_dir)
        except Exception, e:
            LOG.error(str(e))
    try:
        get_logs_cmd = "scp -i %(ssh_key)s -oUserKnownHostsFile=no -oStrictHostKeyChecking=no root@%(master_ip)s:/var/log/frieda/*.log %(results_dir)s/; mv %(cwd)s/*.log %(results_dir)s" % \
                       {'ssh_key': ssh_key, 'master_ip': os.environ['MASTER_PRIVATE_IP'], 'results_dir': results_dir,
                        'cwd': os.getcwd()}

        status, output = commands.getstatusoutput(get_logs_cmd)
        if status > 0:
            raise Exception("command '%s' failed  with status:%s output:%s" % (get_logs_cmd, status, output))

    except Exception, e:
        LOG.error(str(e))

    end = time.time()
    LOG.info("Run App '%s' Time: %f" % (experiment.app_name, (end - start)))
    LOG.info("\n*****************\nRun App '%s' End\n****************\n" % experiment.app_name)


def set_environment_variable(name, value, default):
    """
    Set the environment variable

    :param name: environment variable name
    :param value: value to set
    :param default: default value to set if no value is present
    """
    if value:
        os.environ[name] = value
    elif name not in os.environ and default:
        os.environ[name] = default


def test_suite(experiments, app_name, fs_type, instance_type, num_vms, config, output_dir="results", data_dir="/data",
               input_dir='input', worker_data_dir="/data", worker_output_dir=None, os_compute_service=None,
               device=None):
    """

    :param experiments: experiments to run (input data size, # of cores)
    :type experiments: list
    :param app_name:
    :param fs_type:
    :param instance_type:
    :param num_vms:
    :param config:
    :param output_dir:
    :param data_dir:
    :param input_dir:
    :param worker_data_dir:
    :param os_compute_service:
    :param device:
    :return:
    """
    # set up the environment variables
    if not worker_output_dir:
        worker_output_dir="%s-output" % app_name
    os.environ['OS_INSTANCE_TYPE'] = str(instance_type)
    os.environ['CONFIG'] = config
    set_environment_variable('DATA_DIR', data_dir, '/data')
    set_environment_variable('INPUT_DATA_DIR', "%s/%s" % (os.environ['DATA_DIR'], input_dir),
                             "%s/%s" % (os.environ['DATA_DIR'], 'input'))
    set_environment_variable('WORKER_DATA_DIR', worker_data_dir, '/data')
    set_environment_variable('WORKER_OUTPUT_DIR', "%s/%s" % (os.environ['WORKER_DATA_DIR'], worker_output_dir), '/data/output')
    set_environment_variable('OS_COMPUTE_SERVICE_NAME', os_compute_service, None)
    set_environment_variable('DEVICE', device, '/dev/vdb')
    os.environ['NAME'] = app_name
    os.environ['SIZE'] = ''

    ssh_key = os.path.expandvars(os.path.expanduser("${HOME}/.ssh/${EC2_KEYPAIR}.pem"))

    LOG.info("Starting the Cluster:  num_workers:%(num_vms)s, instance_type:%(instance_type)s" %
             {'num_vms': num_vms, 'instance_type': instance_type})

    config_file = "%(config)s.yaml" % {'config': config}
    if not os.path.exists(config_file):
        raise Exception("%s does not exists" % config_file)
    execute_integration(config_file, 'auto', " ".join(["%03d" % i for i in range(num_vms + 1)]))
    LOG.info("Cluster Started")

    for e in experiments:
        LOG.info("\n*****************\n%s Start\n****************\n" % str(e))
        os.environ['SIZE'] = str(e.input_size)
        run_experiment(e, config_file, output_dir, ssh_key)
        LOG.info("\n*****************\n%s End\n****************\n" % str(e))

    LOG.info("Terminating the Cluster")
    execute_integration(config_file, 'stop')
    LOG.info("Cluster Terminated")


if __name__ == "__main__":
    if len(sys.argv) < 6:
        LOG.info("Usage: %s <app> <fs_types> <instance_types> <input_sizes> <num_cores>" % sys.argv[0])
        exit()
    pdb.set_trace()
    main(sys.argv)
