import sys
import argparse
import time

from globusonline.transfer import api_client


class GlobusOnline(object):

    def __init__(self):

        """

        :param go_pwd: globus online password
        :param go_uname: globus online username
        """

        parser = argparse.ArgumentParser(description='Data Transfer via Globus Online')
        parser.add_argument('-e','--source-end-point', help='Source end point', required=True)
        parser.add_argument('-s','--source-path', help='Source data path', required=True)
        parser.add_argument('-d','--destination-path', help='Destination data path', required=True)
        parser.add_argument('-c','--certificate', help='certificate', required=True)
        parser.add_argument('-u','--user', help='user', required=True)
        
        args = vars(parser.parse_args())  

        self.user = args['user']
        self.proxy_cert = args['certificate']  

        self.source_ep = args['source_end_point']        
        self.source_path = args['source_path']        
        self.dest_ep = "%s#master" % self.user
        self.dest_path = args['destination_path']     

        
        self.api = api_client.TransferAPIClient(self.user,
                                cert_file=self.proxy_cert,
                                key_file=self.proxy_cert,
                                base_url="https://transfer.api.globusonline.org/v0.10")




    def transfer(self):

        """
        Transfers from source URL to machine to target_url

        :param source_url: source endpoint details
        :param target_url: target endpoint details
        """

        transfer_start = time.time()
        print("transfer from %s:%s to %s:%s"%(self.source_ep, self.source_path, self.dest_ep, self.dest_path))


        transfer_id = self.api.submission_id()[2]["value"]
        print("Transfer ID: %s"%transfer_id)

        transfer = api_client.Transfer(transfer_id, self.source_ep, self.dest_ep,
                                       deadline=None, sync_level=None, label=None)

        if self.__is_remote_directory(self.source_ep, self.source_path):
            transfer.add_item(source_path=self.source_path, destination_path=self.dest_path, recursive=True )
        else:
            transfer.add_item(source_path=self.source_path, destination_path=self.dest_path, recursive=False )

        result = self.api.transfer(transfer)
        task_id = result[2]["task_id"]
        print("Transfer Request Result: %s Task ID: %s"%(str(result), task_id))
        self.__wait_for_task(task_id)
        print("Task ID: %s Time: %d sec"%(transfer_id, (time.time()-transfer_start)))
    

    def __is_remote_directory(self, ep, path):

        """

        :param url: end point url to check whether the path is directory or not
        :return:
        """
        try:
            result = self.api.endpoint_ls(ep, path)      
            print("GO EP: %s Directory: %s Creation Result: %s"%(ep, path, str(result)))
            return True  
        except:
            pass       
        return False

      
    
    def __wait_for_task(self, task_id, timeout=None):
        """

        :param task_id: globus generated task id
        :param timeout: timeout
        :return: True if task is completed/ False if task is still executing after timeout
        """
        status = "ACTIVE"
        while (timeout==None or timeout > 0) and status == "ACTIVE":
            code, reason, data = self.api.task(task_id, fields="status")
            status = data["status"]
            time.sleep(1)
            if timeout!=None:
                timeout -= 1

        if status != "ACTIVE":
            print "Task %s complete!" % task_id
            return True
        else:
            print "Task still not complete after %d seconds" % timeout
            return False

    
    
if __name__ == "__main__":

    go = GlobusOnline()

    # Transfer Data

    go.transfer()