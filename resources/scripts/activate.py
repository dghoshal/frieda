import sys
import argparse

from globusonline.transfer.api_client import x509_proxy
from globusonline.transfer import api_client


def activate_source_ep():

    """

    :param activate_url: end point to be activated
    :return:
    """

    parser = argparse.ArgumentParser(description='Globus Online end point Activation')
    parser.add_argument('-e','--source-end-point', help='Source end point', required=True)
    parser.add_argument('-l','--credential-lifetime', help='Credential Lifetime in hours', required=True)
    parser.add_argument('-c','--certificate', help='certificate', required=True)
    parser.add_argument('-u','--user', help='user', required=True)
    
    args = vars(parser.parse_args())  

    proxy_cert = args['certificate']  
    user = args['user']
    lifetime = int(args['credential_lifetime'])
    source_ep = args['source_end_point']


    
    api = api_client.TransferAPIClient(user,
                            cert_file=proxy_cert,
                            key_file=proxy_cert,
                            base_url="https://transfer.api.globusonline.org/v0.10")

    _, _, reqs = api.endpoint_activation_requirements(source_ep, type="delegate_proxy")

    
    public_key = reqs.get_requirement_value("delegate_proxy", "public_key")
    proxy = x509_proxy.create_proxy_from_file(proxy_cert, public_key, lifetime)
    reqs.set_requirement_value("delegate_proxy", "proxy_chain", proxy)
    status, message, data = api.endpoint_activate(source_ep, reqs)

    print data['message']

activate_source_ep()
