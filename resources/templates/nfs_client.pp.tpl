Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }


notify { "nfs-common":
    message=>"Preparing NFS Client",
}

$exportdir='/export'
$datadir='/data'
$nfs_server="%(MASTER_PRIVATE_IP)s"

$nfs_pkgs = $operatingsystem ? {
  centos => 'nfs-utils',
  ubuntu => 'nfs-common'
}


notify { "nfs-common-$datadir":
	message=>"Preparing NFS Client $datadir",
}

package {$nfs_pkgs  :
    ensure=>installed,
}

file {$datadir:
    ensure=>directory,

}

# Mount the file system
mount { $datadir:
        fstype=>'nfs',
        options=>'proto=tcp,port=2049',
        device=>"$nfs_server:$exportdir$datadir",
        ensure=>mounted,
        require=> [Package[$nfs_pkgs],File[$datadir]],

}