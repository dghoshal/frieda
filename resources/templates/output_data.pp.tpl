Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "output-data-notification":
		message=>"Preparing output data",
	}
	
	
file { ['%(output_root_dir)s','%(output_root_dir)s/%(output_dir)s']:
	ensure=>directory,
	owner=>"%(user)s",
	mode=>0666,
}
