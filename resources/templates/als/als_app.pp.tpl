$src_dir="%(als_user_dir)s/als"
Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "als-notification":
		message=>"Installing als app: ${svn_als_url}",
	}
	
	
package { ['python','python-dev','python-setuptools','python-pip','git','subversion','gcc','python-opencv']:
	ensure=>installed,
}

# This is not specific to this installation and should probably be put in a place
# where it can be reused.
define install_pkg ($pkgname, $timeout= 1200, $extra_pip_args = "") {
  exec {
   "InstallPkg_$pkgname":
   command => "pip install $extra_pip_args $pkgname",
   timeout => $timeout,
   require => Package["python-pip"];
  }
}

#Install ALS app (PIL and Pillow deprecated to cv2?)
install_pkg { "install_numpy_def":
   pkgname=>"numpy",
   timeout=>12000,
   require=>Package['python-pip'],
}
install_pkg { "install_PIL_def":
   pkgname=>"PIL",
   timeout=>12000,
   require=>Package['python-pip'],
}
install_pkg { "install_Image":
   pkgname=>"Pillow",
   timeout=>12000,
   require=>Package['python-pip'],
}
