define add_user ($usersdir="/home" ) {


  group { $name:
        ensure => present,
  }
  user { $name:
	ensure=> present,
	gid=>"$name",
	shell=>'/bin/bash',
	home=>"$usersdir/${name}",
	managehome=>true,
    require => [Group[$name],File[$usersdir]],
  }
}

file { "%(user_dir)s":
    ensure => directory,
  }
add_user {%(user_list)s:
    usersdir => "%(user_dir)s", 
    require => File["%(user_dir)s"]}

