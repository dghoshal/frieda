$go_user="%(go_user)s"
$install_location="%(user_dir)s"
$go_download="wget https://s3.amazonaws.com/connect.globusonline.org/linux/stable/globusconnect-latest.tgz"

exec { $go_download:
  cwd     => $install_location,
  path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin",
  creates => "$install_location/globusconnect-latest.tgz"
}

$unpack_go="tar xzf globusconnect-latest.tgz"

exec { $unpack_go:
  cwd     => $install_location,
  path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin",
  #creates => "$install_location/globusconnect-1.6",
  onlyif => ["test -f $install_location/globusconnect-latest.tgz"],
  subscribe   => Exec[$go_download],
  refreshonly => true
}


#$list_master="ssh -i ~/.ssh/globuskey -o StrictHostKeyChecking=no $go_user@cli.globusonline.org  endpoint-list master"
#$remove_master="ssh -i ~/.ssh/globuskey -o StrictHostKeyChecking=no $go_user@cli.globusonline.org  endpoint-remove master"

#exec { $remove_master:
#  path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin",
#  #onlyif => $list_master,
#  logoutput => "true"
#}

$install_go_transfer_api="easy_install-2.6 globusonline-transfer-api-client"
exec { $install_go_transfer_api:
  path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin",
  logoutput => "true"
}
