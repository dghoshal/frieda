Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "log-data-notification":
		message=>"Preparing log data",
	}
	
	
file { ['%(log_root_dir)s','%(log_root_dir)s/%(log_dir)s']:
	ensure=>directory,
	owner=>"%(user)s",
	mode=>0666,
}
