Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "block-storage-notification-data":
		message=>"Preparing data",
	}

$device="%(device)s"

# Make the file system if it hasn't been made already
exec { "mkfs $device":
       onlyif => "test -z `file -s $device* | grep ext`",
       timeout => 0,
}

