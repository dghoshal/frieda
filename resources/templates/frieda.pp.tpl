# FRIEDA Installation

$frieda_user="%(frieda_user)s"
$frieda_user_dir="%(frieda_user_dir)s"
$frieda_src_dir="%(frieda_user_dir)s/FRIEDA"
$frieda_config_dir="%(frieda_user_dir)s/.frieda"
$frieda_config_file="${frieda_config_dir}/config"
$ssh_keyfile="%(ssh_keyfile)s"

$friedaConfig = "[main]
ssh_user: $frieda_user 
ssh_key: $frieda_user_dir/.ssh/$ssh_keyfile


[control]
STDOUT_FILE: /var/log/frieda/client.log
STDERR_FILE: /var/log/frieda/client.err

[master]"
Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }


$python = $operatingsystem ? {
  centos => 'python',
  ubuntu => 'python'
}

$python_devel = $operatingsystem ? {
  centos => 'python-devel',
  ubuntu => 'python-dev'
}


package { [$python,$python_devel,'git','gcc','subversion']:
	ensure=>installed,
}

class frieda_src{
file {  $frieda_src_dir:
	ensure=>directory,
	owner=>$frieda_user,
	mode=>0644,
}
}

file {  $frieda_config_dir:
	ensure=>directory,
	owner=>$frieda_user,
	mode=>0644,
}

# Need to do something with the template in here.
file {  $frieda_config_file:
	ensure=>present,
	owner=>$frieda_user,
	mode=>0644,
	content=>$friedaConfig,
	require=>File[$frieda_config_dir],
}

file { '/var/log/frieda':
	ensure=>directory,
	owner=>$frieda_user,
	mode=>0644,
}
		

include frieda_src
exec { "install_friedaf":
   command => "$python setup.py install",
   cwd => $frieda_src_dir,
   timeout=>1200,
   require => [Class["frieda_src"],Package[$python,$python_devel,'git','gcc','subversion']];
}
# END frieda.pp
