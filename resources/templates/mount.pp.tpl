Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "block-storage-notification-data":
		message=>"Preparing data",
	}

$device="%(device)s"
$mntdir='/data'
$inputdir="/data/input"
$fstype='ext2'
	
# Make the file system if it hasn't been made already
#exec { "mkfs $device":
#       onlyif => "test -z `file -s $device* | grep ext`",
#       timeout => 0,
#}

# Create the mount directory
file { [$mntdir, $inputdir]:
	ensure=>directory,
	owner=>"root",
	mode=>0666,
}


# Mount the file system
mount { $inputdir:
        fstype=>$fstype,
        options=>'rw',
        device=>"$device",
        ensure=>mounted,
        require=> [File[$inputdir]],
#        subscribe=>Exec["mkfs $device"],
        

}
