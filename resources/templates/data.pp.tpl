Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "data-notification":
		message=>"Preparing data directories",
	}
	
	
file { %(data_dirs)s:
	ensure=>directory,
	owner=>"%(user)s",
	mode=>0666,
}
