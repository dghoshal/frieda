Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "input-data-notification":
		message=>"Preparing input data",
	}
	
	
file { ['%(input_root_dir)s','%(input_root_dir)s/%(input_dir)s']:
	ensure=>directory,
	owner=>"%(user)s",
	mode=>0666,
}
