#! /usr/bin/env python

import cv2
import numpy
import sys


def sumSquaredDifferences(img1,img2):
    """ Compute the sum of squared differences for each pair of frames in video
        volume.
            -The matrix is symmetrical

    Args:
        video_volume (numpy.ndarray): A 4D numpy array with dimensions
                                      (num_frames, rows, cols, 3). This can be
                                      produced by the videoVolume function.

    Returns:
        output (numpy.ndarray): A square 2d numpy array of dtype float.
                                output[i,j] should contain  the sum of square
                                differences between frames i and j. This matrix
                                is symmetrical with a diagonal of zeros. The
                                values should be np.float.
    """
    
    # WRITE YOUR CODE HERE.
    """
    for i in range(length):
        cur_frame = video_volume[i]
        for n in range(slim,length):
            comparison_frame = video_volume[n]
            ssd = np.sum((cur_frame.astype('float')-comparison_frame.astype('float'))**2)
            output[i][n] = abs(ssd)
            output[n][i]= abs(ssd)
        slim+=1
    """
    output = numpy.sum((img1.astype('float')-img2.astype('float'))**2)
    return output

# Returns zero if two images are equal 
def main():
  print sys.argv[1] , sys.argv[2]
  img1 = cv2.imread(sys.argv[1])
  img2 = cv2.imread(sys.argv[2])
  print type(img1),':',type(img2)
  print 'Comparing sizes'
  if img1.size != img2.size:
    print "Doesn't match"
    return -1

  print 'Comparing data'
  s = 0
  s = sumSquaredDifferences(img1,img2)
  print "Sum Squared: ", s
  if s == 0:
    print 'Matched'
    outfile = open("matched_pairs.txt", "a")
    outfile.write(sys.argv[1] + " == " + sys.argv[2] + "\n")
    outfile.close()
  else:
    print 'Not matched'

if __name__ == "__main__":
  #sys.exit(main())
  main()
