#!/usr/bin/env python
'''
Enables to easily run the FRIEDA data management system on private and public clouds
#GOALS:
--Variables do not have to be set
--Lets the user know if the variable exists
--Can be run more than once without worry 
--Pure python without bash script

Version: 2.0
Author: Eugen Feller <efeller@lbl.gov>
Update_by: William Fox <wfox@lbl.gov>
Updated_last: 11-10-2015
'''
import re
import traceback
import argparse
import os
import io
import logging
import yaml
import paramiko
import frieda
import sys
import subprocess
import time
from boto import ec2
from urlparse import urlparse
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.drivers.ec2 import EC2NodeLocation
from libcloud.compute.drivers.ec2 import ExEC2AvailabilityZone
from libcloud.compute.base import StorageVolume, NodeState, NodeImage, NodeSize
from time import sleep

""" Logging """
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
#silence parmiko debugging output
logging.getLogger("paramiko").setLevel(logging.WARNING)

""" Data partitioning """
DATA_PARTITIONING_MODES = ['none', 'exclusive','datacoordinator']

""" Volume settings """
VOLUME_STATUS_POLLING_INTERVAL = 2
VOLUME_ATTACH_TIME = 10

"""Output"""
def print_log(update):
    #Comment out ifno updates wanted
    print update
    return 0



"""OUTDATED"""
def get_cloud_provider():
    return os.environ["CLOUD_PROVIDER"]

"""OUTDATED"""
def get_cloud_region():
    return os.environ["CLOUD_REGION"]

"""OUTDATED"""
def get_cloud_availability_zone():
    return os.environ["CLOUD_AVAILABILITY_ZONE"]

"""OUTDATED"""
def get_ec2_access_key():
    return os.environ["EC2_ACCESS_KEY"]


def get_ec2_secret_key():
    return os.environ["EC2_SECRET_KEY"]


def get_ec2_keypair():
    return os.getenv("EC2_KEYPAIR")


def get_ec2_url():
    return os.environ['EC2_URL']


def get_frieda_home():
    return os.environ['FRIEDA_HOME']




def connect_to_libcloud(provider, region):
    logger.info("Estabilishing connection to cloud provider %s", provider)
    connection = None
    if provider == "ec2":
        logger.info("Selecting region %s", region)
        if region == "us-west-1":
            Driver = get_driver(Provider.EC2_US_WEST)
        else:
            raise Exception("Unsupported region selected!")
        ec2_access_key = get_ec2_access_key()
        ec2_secret_key = get_ec2_secret_key()
        connection = Driver(ec2_access_key, ec2_secret_key)
    elif provider == "euca":
        Driver = get_driver(Provider.EUCALYPTUS)
        ec2_access_key = get_ec2_access_key()
        ec2_secret_key = get_ec2_secret_key()
        url = urlparse(get_ec2_url())
        connection = Driver(ec2_access_key,
                            ec2_secret_key,
                            host=url.hostname,
                            port=url.port,
                            path=url.path,
                            secure=False)
    else:
        raise Exception("Unsupported cloud provider selected")

    if connection is None:
        raise Exception("Connection is NONE")

    return connection


def mount_volume(instance_ip, device_name, ssh_key, ssh_user, mount_point):
    mount = "sudo mkdir %s; sudo mount %s %s; sudo chown -R %s:%s %s" % (
        mount_point, device_name, mount_point, ssh_user, ssh_user, mount_point)
    logger.info("Mounting volume: %s", mount)
    execute_ssh(mount, ssh_user, ssh_key, [instance_ip])


def get_master_instance(connection, keyname):
    instances = get_non_terminated_instances(connection, keyname)
    if len(instances) == 0:
        raise Exception("No instances found!")

    master = instances[0]
    return master


def get_all_instance_ids(connection, keyname):
    instance_objects = get_non_terminated_instances(connection, keyname)
    instance_ids = [instance.id for instance in instance_objects]
    return instance_ids


def get_device_name(template):
    device_name = get_value_from_tag(template, "device_name")
    return device_name


def get_ssh_user(template):
    ssh_user = get_value_from_tag(template, "ssh_user")
    return ssh_user


def get_image_id(template):
    image_id = get_value_from_tag(template, "image_id")
    return image_id


def get_instance_type(template):
    instance_type = get_value_from_tag(template, "instance_type")
    return instance_type


def get_worker_data_directory(template):
    directory = get_value_from_tag(template, "worker_data_dir")
    return directory


def get_volume_id(template):
    volume_id = get_value_from_tag(template, "volume_id")
    return volume_id


def get_master_data_directory(template):
    mount_point = get_value_from_tag(template, "master_data_dir")
    return mount_point


def get_destination_directory(template):
    """
    Returns the destination directory for the output files.
    The destination directory is found in the configuration
    template.
    """
    return get_value_from_tag(template, "destination_dir")


def get_destination(template):
    """
    Returns the destination  for the output files.
    The destination is found in the configuration
    template.
    """
    return get_value_from_tag(template, "destination")


def get_uid(template):
    """
    Returns the uid  for the output files.
    The uid is found in the configuration
    template.
    """
    return get_value_from_tag(template, "uid")


def copy_data(source, template, ssh_user, ssh_key, instance_ip):
    master_data_directory = get_master_data_directory(template)
    data_directory = os.path.split(os.path.dirname(source + "/"))[1]
    master_data_directory = master_data_directory + "/" + data_directory + "/" + "input"
    mkdir_command = "mkdir -p %s" % master_data_directory
    execute_ssh(mkdir_command, ssh_user, ssh_key, [instance_ip])
    execute_scp(source + "/*", master_data_directory, ssh_user, ssh_key, [instance_ip])


def _build_code(directory, command, ssh_user, ssh_key, instances):
    logger.info("Building application using %s", command)
    bexec = "cd %s; %s" % (directory, command)
    execute_ssh(bexec, ssh_user, ssh_key, instances)


def get_default_ssh_key():
    return "%s/.ssh/%s.pem" % (os.getenv("HOME"), get_ec2_keypair())


def get_remote_home_directory(ssh_user):
    return "/home/%s" % ssh_user


def upload_app(tar_archive, ssh_key, provider, bexec, keyname, region):
    connection = connect_to_libcloud(provider, region)
    instances = get_worker_instance_ips(connection, keyname)
    template = get_template(provider)
    ssh_user = get_ssh_user(template)
    destination = get_remote_home_directory(ssh_user)
    master_instance = get_master_instance(connection, keyname)
    output = execute_scp(tar_archive, destination, ssh_user, ssh_key, [master_instance.public_ips[0]])

    execute_scp(ssh_key, destination + "/.ssh/", ssh_user, ssh_key, [master_instance.public_ips[0]])
    for ip in get_worker_instance_private_ips(connection, keyname):
        scpcommand = "scp -o StrictHostKeyChecking=no -i ~/.ssh/" + ssh_key.split("/")[-1] + " " + \
                     tar_archive.split("/")[-1] + " " + ip + ":"
        output = execute_ssh(scpcommand, ssh_user, ssh_key, [master_instance.public_ips[0]], False)

    basename = os.path.basename(tar_archive)
    unpack = "tar -xzvf %s/%s" % (destination, basename)
    execute_ssh(unpack, ssh_user, ssh_key, instances, True)

    if bexec is not None:
        directory = destination + "/" + basename.split('.')[0]
        _build_code(directory, bexec, ssh_user, ssh_key, instances)


def upload_data(source, ssh_key, provider, keyname, region):
    connection = connect_to_libcloud(provider, region)
    master_instance = get_master_instance(connection, keyname)
    instance_ip = master_instance.public_ips[0]
    template = get_template(provider)
    volume_id = get_volume_id(template)
    mount_point = get_master_data_directory(template)
    logger.info("Uploading application data to volume %s (%s) on instance %s", volume_id, mount_point, instance_ip)

    device_name = get_device_name(template)
    ssh_user = get_ssh_user(template)

    mount_volume(instance_ip=instance_ip,
                 ssh_user=ssh_user,
                 ssh_key=ssh_key,
                 mount_point=mount_point,
                 device_name=device_name)

    copy_data(source=source,
              template=template,
              ssh_user=ssh_user,
              ssh_key=ssh_key,
              instance_ip=instance_ip)


def cleanup_workers(worker_ips, ssh_user, ssh_key, template):
    """
    This first transfers the application outputs to the output (if specified)
    directory and then removes them from the workers.

    :param worker_ips: the public ip addresses of the worker nodes
    :type worker_ips: list
    :param ssh_user: the worker node username
    :param ssh_key: the ssh key filename
    :param template: the string representation of the FRIEDA configuration template

    """
    worker_directory = get_worker_data_directory(template)
    logger.info("Cleaning up %s directory on workers: %s", worker_directory, worker_ips)
    command = "rm -Rf %s/*" % worker_directory
    execute_ssh(command, ssh_user, ssh_key, worker_ips)

"""TRYING TO FIX COMPLEXITY"""
def get_value_from_tag(template, tag):
    logger.info("Getting value for tag %s in template %s", tag, template)
    template_value = open(template).read() % os.environ
    if tag == "worker_data_dir":
        match = re.search("(?<=worker_data_dir: )(.*)", template_value)
    elif tag == "ssh_user":
        match = re.search("(?<=ssh_user: )(.*)", template_value)
    elif tag == "instance_type":
        match = re.search("(?<=instance_type: )(.*)", template_value)
    elif tag == "device_name":
        match = re.search("(?<=device: )(.*)", template_value)
    elif tag == "volume_id":
        match = re.search("vol-[a-zA-Z0-9]+", template_value)
    elif tag == "image_id":
        match = re.search("(?<=image_id: )(.*)", template_value)
    elif tag == "master_data_dir":
        match = re.search("(?<=master_data_dir: )(.*)", template_value)
    elif tag == "destination_dir":
        match = re.search("(?<=destination_dir: )(.*)", template_value)
    elif tag == "destination":
        match = re.search("(?<=destination: )(.*)", template_value)
    elif tag == "uid":
        match = re.search("(?<=uid: )(.*)", template_value)
    else:
        raise Exception("Unknown tag selected!")

    if match:
        return match.group()

    raise Exception("Unable to find any matches for the selected tag " + tag)

"""get_value_from_tag replacement"""
def extract_yaml_value(template,tag):
    return tag


def create_local_directory(directory):
    """
    Create a directory on the local machine
    """
    if not os.path.isdir(directory):
        os.mkdir(directory)


def run_application(input, output, file_prefix, file_suffix, partitioning, execution_command, gb_per_job, ssh_key,
                    provider, keyname, region):
    template = get_template(provider)
    logger.info("Running application with template %s, gb_per_job: %d", template, gb_per_job)
    connection = connect_to_libcloud(provider, region)
    instances = get_all_instance_ids(connection, keyname)

    change_max_size(template, gb_per_job)
    change_execution_command(template, partitioning, execution_command)
    change_file_prefix(template, file_prefix)
    change_file_suffix(template, file_suffix)

    # Determine the destination from the given output parameter
    change_data_destination_directory(template, output)
    change_data_partitioning_mode(template, partitioning)

    mount_point = get_master_data_directory(template)
    change_master_data_directory(template, mount_point + "/" + input)

    friedaexecute = "frieda all -c %s -i %s" % (template, ','.join(instances))
    execute_local_command(friedaexecute)

    ssh_user = get_ssh_user(template)
    worker_ips = get_worker_instance_ips(connection, keyname)
    cleanup_workers(worker_ips, ssh_user, ssh_key, template)
    change_master_data_directory(template, mount_point)


def cleanup_nodes(ssh_key, provider, keyname, region):
    template = get_template(provider)
    connection = connect_to_libcloud(provider, region)
    instances = get_all_instance_ids(connection, keyname)
    ssh_user = get_ssh_user(template)
    worker_ips = get_worker_instance_ips(connection, keyname)
    cleanup_workers(worker_ips, ssh_user, ssh_key, template)


def get_non_terminated_instances(connection, keyname):
    nodes = connection.list_nodes()
    instances = []

    provider = get_cloud_provider()
    template = get_template(provider)
    image_id = get_image_id(template)
    for i in nodes:
        if i.state != NodeState.TERMINATED and i.extra['key_name'] == keyname:
            if image_id and 'image_id' in i.extra:
                if i.extra['image_id'] == image_id:
                    instances.append(i)
            else:
                instances.append(i)
    return instances


def provision_spot_instances(number_of_instances, region, keyname, instance_type, spot_price, image_id, security_group,
                             availability_zone):
    conn = ec2.connect_to_region(region, aws_access_key_id=get_ec2_access_key(),
                                 aws_secret_access_key=get_ec2_secret_key())
    logger.info("Provisioning spot ec2 instances")
    conn.request_spot_instances(price=spot_price,
                                image_id=image_id,
                                instance_type=instance_type,
                                count=number_of_instances,
                                key_name=keyname,
                                security_groups=[security_group],
                                placement=availability_zone)


def get_node_location(provider, region, availability_zone):
    """
    Node location and availability zones are currently only supported for ec2
    """

    if provider == "ec2":
        availability = ExEC2AvailabilityZone(name=availability_zone, zone_state="available", region_name=region)
        location = EC2NodeLocation(id="", name="", availability_zone=availability, country="", driver="")
        return location
    else:
        return None


def provision_regular_instances(number_of_instances, provider, region, availability_zone, instance_type, image_id,
                                security_group, keyname):
    connection = connect_to_libcloud(provider, region)
    image = NodeImage(id=image_id, name="", driver="")
    # size = NodeSize(id=instance_type, name="", ram=None, disk=None, bandwidth=None, price=None, driver="")
    sizes = connection.list_sizes()
    size = [s for s in sizes if s.id == instance_type][0]
    location = get_node_location(provider, region, availability_zone)

    for _ in range(number_of_instances):
        node = connection.create_node(name="",
                                      ex_securitygroup=security_group,
                                      ex_keyname=keyname,
                                      image=image,
                                      location=location,
                                      size=size)
        logger.info("Waiting for the instance %s to become available", node.id)
        nodes = get_non_terminated_instances(connection, keyname)
        connection.wait_until_running(nodes)


def provision_instances(number_of_instances, provider, region, keyname, spot_price, security_group, availability_zone,
                        is_spot):
    logger.info("Provisioning instances on provider %s", provider)
    template = get_template(provider)
    image_id = get_image_id(template)
    instance_type = get_instance_type(template)
    if is_spot:
        provision_spot_instances(number_of_instances=number_of_instances,
                                 spot_price=spot_price,
                                 region=region,
                                 availability_zone=availability_zone,
                                 keyname=keyname,
                                 instance_type=instance_type,
                                 image_id=image_id,
                                 security_group=security_group)
    else:
        provision_regular_instances(number_of_instances=number_of_instances,
                                    provider=provider,
                                    region=region,
                                    keyname=keyname,
                                    instance_type=instance_type,
                                    image_id=image_id,
                                    security_group=security_group,
                                    availability_zone=availability_zone)
    logger.info("Provisioning finished")


def list_instances(provider, region, keyname):
    connection = connect_to_libcloud(provider, region)
    instances = get_non_terminated_instances(connection, keyname)

    if len(instances) == 0:
        logger.info("No instance running")
        return

    logger.info("Listing instances IPs")
    for instance in instances:
        logger.info("%s - %s", instance.id, instance.public_ips)


def get_worker_instance_ips(connection, keyname):
    instances = get_non_terminated_instances(connection, keyname)[1:]
    instance_ips = [instance.public_ips[0] for instance in instances]
    return instance_ips


def get_worker_instance_private_ips(connection, keyname):
    instances = get_non_terminated_instances(connection, keyname)[1:]
    instance_ips = [instance.private_ips[0] for instance in instances]
    return instance_ips


def execute_ssh(command, ssh_user, ssh_key, hostnames, is_parallel=False):
    logger.info("Executing remote command %s on %s with username %s and key %s", command, hostnames, ssh_user, ssh_key)

    if is_parallel:
        hosts = ""
        for i in hostnames: hosts = hosts + " " + i
        options = "pssh -t 0 -P -l %s -H %s -x \"-t -t -o StrictHostKeyChecking=no -i %s\" '%s'" % (
            ssh_user, ' -H '.join(hostnames), ssh_key, command)
        print
        "Executing " + options
        out = execute_local_command(options)
        logger.info(out)
        return

    for hostname in hostnames:
        options = "ssh -t -i %s -o StrictHostKeyChecking=no %s@%s '%s'" % (ssh_key, ssh_user, hostname, command)
        out = execute_local_command(options)
        logger.info(out)


def execute_scp(source, destination, ssh_user, ssh_key, hostnames, is_parallel=False):
    logger.info("Data Transfer - source %s, destination %s, username %s, ssh_key %s to %s", source, destination,
                ssh_user,
                ssh_key, hostnames)

    if is_parallel:
        options = "pscp -r -l %s -H %s -x \"-o StrictHostKeyChecking=no -i %s\" %s %s" % (
            ssh_user, ' -H '.join(hostnames), ssh_key, source, destination)
        execute_local_command(options)
        return

    for hostname in hostnames:
        options = "scp -i %s -r %s %s@%s:%s" % (ssh_key, source, ssh_user, hostname, destination)
        execute_local_command(options)


def terminate_instances(provider, region, keyname):
    connection = connect_to_libcloud(provider, region)
    instances = get_non_terminated_instances(connection, keyname)
    if len(instances) == 0:
        logger.info("Nothing to terminate")
        return

    logger.info("Terminating all instances on %s for %s", provider, keyname)
    for i in instances:
        connection.destroy_node(i)

    logger.info("All instances terminated")


def get_template(provider):
    base = get_frieda_home()
    if provider == "euca":
        return base + "/" + "default_euca.yaml"
    elif provider == "ec2":
        return base + "/" + "default_ec2.yaml"





def change_max_size(template, max_size):
    modify_frieda_template(template, "max_size", max_size)


def change_file_prefix(template, file_prefix):
    file_prefix = "['" + file_prefix + "']"
    modify_frieda_template(template, "files", file_prefix)


def change_file_suffix(template, file_suffix):
    modify_frieda_template(template, "suffix", file_suffix)


def change_execution_command(template, partitioning, command):
    ssh_user = get_ssh_user(template)
    directory = get_remote_home_directory(ssh_user)

    if partitioning == "none":
        command = command + " \$1"
    elif partitioning == "exclusive":
        command = command + " \$1 \$2"

    command = "%s/%s" % (directory, command)
    modify_frieda_template(template, "command", command)


def change_data_destination_directory(template, output):
    """
    Change the destination directory.  This may be on of the following
    the formats:

     * USER@HOST:FILE_PATH - updates FRIEDA template with uid = USER, destination = HOST, destination_dir = FILE_PATH
     * HOST:FILE_PATH - updates FRIEDA template with destination = HOST, destination_dir = FILE_PATH
     * FILE_PATH - updates FRIEDA template with destination_dir = FILE_PATH

    :param template: the string representation of the FRIEDA configuration template
    :param output: the output directory

    """
    output_split = output.split("@")
    output_path = output
    if len(output_split) == 2:
        modify_frieda_template(template, "uid", output_split[0])
        output_path = output_split[1]
    elif not get_uid(template):
        raise Exception("'output' {} is an invalid. There is no user set. Try USER@HOST:FILE_PATH. ".format())

    output_split = output_path.split(":")
    if len(output_split) == 1:
        if not get_destination(template):
            raise Exception(
                "'output' {} is an invalid. There is no 'destination' host set. Try HOST:FILE_PATH. ".format(output))
        modify_frieda_template(template, "destination_dir", output_split[0])
    elif len(output_split) == 2:
        modify_frieda_template(template, "destination_dir", output_split[1])
        modify_frieda_template(template, "destination", output_split[0])
    else:
        raise Exception(
            "'output' {} is an invalid format.  Must be USER@HOST:FILE_PATH, HOST:FILE_PATH or FILE_PATH.".format(
                output))


def change_data_partitioning_mode(template, mode):
    modify_frieda_template(template, "data_partition", mode)


def change_master_data_directory(template, directory):
    modify_frieda_template(template, "master_data_dir", directory)


def change_volume_id(template, volume_id):
    modify_frieda_template(template, "volume_id", volume_id)


def modify_frieda_template(template, tag, value):
    logger.info("Adjusting the FRIEDA template %s for tag %s with value %s", template, tag, value)
    input = open(template).read()
    output = open(template, 'w')

    if tag == "max_size":
        line = re.sub("max_size:.*", "max_size: %d" % value, input)
    elif tag == "volume_id":
        line = re.sub("vol-[a-zA-Z0-9]*", "%s" % value, input)
    elif tag == "master_data_dir":
        line = re.sub("master_data_dir:.*", "master_data_dir: %s" % value, input)
    elif tag == "files":
        line = re.sub("files:.*", "files: %s" % value, input)
    elif tag == "suffix":
        line = re.sub("suffix:.*", "suffix: %s" % value, input)
    elif tag == "data_partition":
        line = re.sub("data_partition:.*", "data_partition: %s" % value, input)
    elif tag == "destination_dir":
        line = re.sub("destination_dir:.*", "destination_dir: %s" % value, input)
    elif tag == "destination":
        line = re.sub("destination:.*", "destination: %s" % value, input)
    elif tag == "command":
        line = re.sub("command:.*", "command: %s" % value, input)
    elif tag == "uid":
        line = re.sub("uid:.*", "uid: %s" % value, input)
    else:
        raise Exception("Unknown tag selected!")

    output.write(line)
    output.close()


def format_volume(ssh_user, device_name, ssh_key, instance_ip):
    format = "sudo mke2fs -t ext3 %s" % device_name
    execute_ssh(format, ssh_user, ssh_key, [instance_ip])


def attach_volume_aws(instance, volume, device_name, connection):
    logger.info("Attempting to attach EBS volume %s to instance %s at device %s", volume.id, instance.id, device_name)
    isattached = connection.attach_volume(instance, volume, device_name)
    if not isattached:
        raise Exception("Failed to attach volume")

    wait_until_aws_volume_ready(volume.id, "attaching", connection)


def create_volume(provider, region, ssh_key, zone, keyname, size):
    connection = connect_to_libcloud(provider, region)
    instance = get_master_instance(connection, keyname)
    template = get_template(provider)
    ssh_user = get_ssh_user(template)
    device_name = get_device_name(template)
    volume_id = None

    if provider == "euca":
        volume_id = create_volume_euca(zone=zone, size=size)
        attach_volume_euca(instance_id=instance.id, volume_id=volume_id, device_name=device_name)
    elif provider == "ec2":
        volume = create_volume_aws(connection=connection, region=region, zone=zone, size=size)
        volume_id = volume.id
        attach_volume_aws(instance=instance, volume=volume, device_name=device_name, connection=connection)

    if volume_id is None:
        raise Exception("Volume is invalid!")

    sleep(VOLUME_ATTACH_TIME)
    format_volume(ssh_user=ssh_user, ssh_key=ssh_key, device_name=device_name, instance_ip=instance.public_ips[0])
    change_volume_id(template, volume_id)


def execute_local_command(command):
    return os.popen(command).read().strip('\n')


def get_euca_volume_status(volume_id):
    status = "euca-describe-volumes | grep \"%s\" | awk -F \' \' \'{print $5}\' | head -n 1" % (volume_id)
    return execute_local_command(status)


def get_aws_volume_status(volume_id, connection):
    volumes = connection.list_volumes()
    for volume in volumes:
        if volume.id == volume_id:
            return volume.extra['state']

    raise Exception("No volume status found!")


def wait_until_aws_volume_ready(volume_id, volume_status, connection):
    status = get_aws_volume_status(volume_id, connection)
    while status == volume_status:
        logger.info("%s volume %s! Please wait!", volume_status, volume_id)
        sleep(VOLUME_STATUS_POLLING_INTERVAL)
        status = get_aws_volume_status(volume_id, connection)

    if status == "available":
        logger.info("Volume is available!")
    elif status == "in-use":
        logger.info("Volume is in-use/attached!")
    else:
        logger.info("Volume %s failed, status: %s", volume_status, status)


def wait_until_euca_volume_ready(volume_id, volume_status):
    status = get_euca_volume_status(volume_id)
    while status == volume_status:
        logger.info("%s volume %s! Please wait!", volume_status, volume_id)
        sleep(VOLUME_STATUS_POLLING_INTERVAL)
        status = get_euca_volume_status(volume_id)

    if status == "available":
        logger.info("Volume is available!")
    elif status == "in-use":
        logger.info("Volume is in-use/attached!")
    else:
        logger.info("Volume %s failed, status: %s", volume_status, status)


def attach_volume_euca(instance_id, volume_id, device_name):
    logger.info("Attaching volume %s using euca2tools to instance %s", volume_id, instance_id)
    attach_volume = "euca-attach-volume -i %s -d %s %s" % (instance_id, device_name, volume_id)
    execute_local_command(attach_volume)
    wait_until_euca_volume_ready(volume_id, "attaching")


def create_volume_euca(zone, size):
    logger.info("Creating volume using euca2tools in zone %s of size %s", zone, size)
    create_volume = "euca-create-volume -z %s -s %s | awk -F \' \' \'{print $2}\'" % (zone, size)
    volume_id = execute_local_command(create_volume)
    wait_until_euca_volume_ready(volume_id, "creating")
    return volume_id


def create_volume_aws(connection, region, zone, size):
    location = get_node_location("ec2", region, zone)
    volume = None

    try:
        logger.info("Creating volume on AWS zone %s of size %s", zone, size)
        volume = connection.create_volume(size=size, name="", location=location)
        logger.info("Created volume %s", volume.id)
        wait_until_aws_volume_ready(volume.id, "creating", connection)
    except Exception:
        logger.error(traceback.format_exc())
        if volume is not None:
            connection.destroy_volume(volume)

    return volume


"""
#####################################
New Utility Functions
#####################################
"""
def extract_nodes(cloud_parameters):
    """Enters SSH Connection"""
    platform = cloud_parameters['cloud']['platform']
    #Open Stack Specific Connections
    if platform == 'openstack':
        print_log('\tAvailable OpenStack IDs: Do you want to use these for FRIEDA?')
        OpenStack=get_driver(Provider.OPENSTACK)
        driver = OpenStack(os.environ['OS_USERNAME'],
            os.environ['OS_PASSWORD'],
            ex_tenant_name=os.environ['OS_TENANT_NAME'],
            ex_force_auth_url=os.environ['OS_AUTH_URL'],
            ex_force_auth_version='2.0_password')
        nodes = driver.list_nodes()
    #EC2 Specific Connections
    if platform == "ec2":
        print_log('\tAvailable EC2 IDs: Do you want to use these for FRIEDA?')
        EC2=get_driver(Provider.EC2)
        url_parts=urlparse(os.environ['EC2_URL'])
        if url_parts.scheme == 'https': 
            secured = True
        else:
            securder = False
        driver = EC2(os.environ['EC2_ACCESS_KEY'],
            os.environ['EC2_SECRET_KEY'],
                secure= secured,
                host=url_parts.hostname,
                port=url_parts.port,
                path=url_parts.path)
        nodes = driver.list_nodes()
    return nodes


def extract_ids(cloud_parameters):
    id_set=[]
    if cloud_parameters['cloud']['instance_id']==None:
        print_log(":::WARNING::: No ID's detected utilizing:")
        id_set=[]
        nodes=extract_nodes(cloud_parameters)
        for node in nodes:
            print_log("\t"+str(node.id))
            id_set.append(node.id)
        proceed = raw_input("Continue (y) or (n): ")
        if proceed=='n' or proceed=='N'or proceed=='No' or proceed=='no':
            print_log("User EXIT")
            sys.exit()
        elif proceed=='y' or proceed=='Y'or proceed=='YES' or proceed=='yes' or proceed=='Yes':
            id_string=''
            for i in id_set:
                if i == id_set[-1]:
                    id_string=id_string+ str(i)
                else:
                    id_string = id_string+i+','
            cloud_parameters['cloud']['instance_id']=id_string
        else:
            print_log("Invalid Input: EXIT NOW")
            sys.exit()
    else:
        id_set=cloud_parameters['cloud']['instance_id'].split(',')
	print_log("\t\tUtilizing the following ID's:")

    return id_set


def test_connection(cloud_parameters):
    """Enters SSH Connection"""
    platform = cloud_parameters['cloud']['platform']
    id_set=extract_ids(cloud_parameters)
    ssh =paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    #Open Stack Specific Connections
    if platform == 'openstack':
        print_log('\t\tOpenStack Test')
        OpenStack=get_driver(Provider.OPENSTACK)
        driver = OpenStack(os.environ['OS_USERNAME'],
            os.environ['OS_PASSWORD'],
            ex_tenant_name=os.environ['OS_TENANT_NAME'],
            ex_force_auth_url=os.environ['OS_AUTH_URL'],
            ex_force_auth_version='2.0_password')
        nodes = driver.list_nodes()
        ssh_user = cloud_parameters['cloud']['ssh_user']
        key_file=os.path.expanduser("~/.ssh/%s.pem" % cloud_parameters['cloud']['keypair'])
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        
        for node in nodes:
            ips = node.public_ips
            if node.id in id_set:
                for ip in ips:
                    ssh.connect(ip, username=ssh_user,key_filename=key_file)
                    ssh.close()
                    print_log("\t\tSuccessful connection to: "+ssh_user+'@'+ip)
        return None
    #EC2 Specific Connections
    if platform == "ec2":
        print_log('\t\tEC2 Test')
        EC2=get_driver(Provider.EC2)
        url_parts=urlparse(os.environ['EC2_URL'])
        if url_parts.scheme == 'https': 
            secured = True
        else:
            secured = False
        driver = EC2(os.environ['EC2_ACCESS_KEY'],
            os.environ['EC2_SECRET_KEY'],
                secure= secured,
                host=url_parts.hostname,
                port=url_parts.port,
                path=url_parts.path)
        nodes = driver.list_nodes()
        ssh_user = cloud_parameters['cloud']['ssh_user']
        key_file=os.path.expanduser("~/.ssh/%s.pem" % cloud_parameters['cloud']['keypair'])
        for node in nodes:
            ips = node.public_ips
            if node.id in id_set:
                for ip in ips:
                    ssh.connect(ip, username=ssh_user,key_filename=key_file)
                    ssh.close()
                    print_log("\t\tSuccessful connection to: "+ssh_user+'@'+ip)
        return None
    if platform == "ip_address":
        print_log('\t\tIP Address Test')
        key_file=os.path.expanduser("~/.ssh/%s.pem" % cloud_parameters['cloud']['keypair'])
        for ip in id_set:
            ip=ip.split('@')
            ssh.connect(ip[1], username=ip[0],key_filename=key_file)
            ssh.close()
            print_log("\t\tSuccessful conncetion to: "+ip[0]+'@'+ip[1])
    else:
        print_log("Unsupported platform for frieda_wrapper.py: "+str(platform)+"\n\tEXITING NOW")
        sys.exit()

def test_ssh(cloud_parameters,template_list):
    for i in range(len(cloud_parameters)):
        set_system_variables(cloud_parameters[i],template_list[i])
        print_log("\tTesting: "+template_list[i])
        test_connection(cloud_parameters[i])
    return 0

def load_templates(args):
    #load yaml files
    all_template_list=parse_yaml_locations(args.templates)
    print_log("\tUsing templates:")
    cloud_parameters ,template_list=separate_cluster_files(all_template_list)
    if len(template_list)==0:
        print_log("\t\tNo configuration templates found: SYSTEM EXIT")
        sys.exit()
    return template_list,cloud_parameters
def dcn_partition_data(dcn_config):
#   stop any previous data coordinator daemons running on one machine
    sys.argv=['frieda','execute','datacoordinator','-a','stop']
    print_log("Stopping old Data Coordinator Daemons...")
    argu='$:'
    for i in sys.argv: argu= argu +' '+ str(i)
    print_log("\t" + str(argu))

    print_log("\n\n\t\tBEGIN:::DATA COORDINATOR STOP OUTPUT")
    frieda.main()
    print_log("\t\tEND:::DATA COORDINATOR STOP OUTPUT\n\n")
#   set dcn partition CLI args
    sys.argv=['frieda',
        'execute',
        'partition',
        str(dcn_config['application']['input_data']['datacoordinator_dir']), 
        str(dcn_config['application']['input_data']['suffix']), 
        'exclusive', #if non exclusive developed later incorporate option
        os.environ['PWD']+'/'+'semantics',#semantics file
        ]
    print_log("Data Coordinator Deploying...")
    print_log("\t\tpartitioning with:")
    argu='$:'
    for i in sys.argv: argu= argu +' '+ str(i)
    print_log("\t" + str(argu))
#   partition data coordinator
    print_log("\n\n\t\tBEGIN:::DATA COORDINATOR PARTITION OUTPUT")
    frieda.main()
    print_log("\t\tEND:::DATA COORDINATOR PARTITION OUTPUT\n\n")
#   set dcn execute CLI args
    sys.argv=['frieda',
        'execute',
        '-l','log_dcn',
        'datacoordinator',
        '-a','start',
        '-i',str(dcn_config['application']['input_data']['datacoordinator_dir']),
        '-s',os.environ['PWD']+'/'+'semantics',#semantics file
        '-p',str(dcn_config['application']['input_data']['datacoordinator_port']),
        '--src_host',str(dcn_config['application']['input_data']['datacoordinator_host'])
        ]
#   execute data coordinator
    print_log("\t\tBEGIN:::DATA COORDINATOR EXECUTE OUTPUT")
    subprocess.Popen(sys.argv)
    #frieda.main()            
    time.sleep(4)
    print_log("\t\tEND:::DATA COORDINATOR EXECUTE OUTPUT\n\n")
    print_log("\tData Coordinator Deployed and Partitioned")

def partition_phase(args):
        template_list=args.templates
        all_template_list=parse_yaml_locations(args.templates)
        print_log("Extracting Data Corrdinator configuration from:\n\t" + all_template_list[0])
        dcn_config=dcn_extract(all_template_list[0])#returns template yaml with data coordinator characteristics
        dcn_partition_data(dcn_config)
def parse_yaml_locations(templates):
    """
    Still needs:
    decide whether to go into deeper folders (default yes/no)
    --detection of yaml files in same folder (they should not be for now)
        --hopefully will not matter later on
    """
    template_list=[]
    #if None
    if templates == None:
        print_log("No cluster configuration provided: looking locally...")
    #   look for local yaml
        current_directory = os.getcwd()
        #currently looks in all sub directories for all yaml files
            #within the current working directory
        for (dirpath,dirname,filenames) in os.walk(current_directory):
            for f in filenames:
                if f[-5:]=='.yaml':
                    template_list.append(dirpath+'/'+f)
    else:
    #   attempt file parse
        templates=templates.split(',')
        for i in range(len(templates)):
            #test if files exist
            if os.path.isfile(templates[i]):
                if templates[i][-5:]=='.yaml':
                    template_list.append(templates[i])
    return template_list

def check_yaml_type(converted_stream):
    try:
        converted_stream['cloud']['keypair']
        return "cluster"
    except: 
        pass
    try:
        converted_stream[converted_stream.keys()[0]]['keypair']
        return "cloud"
    except:
        raise Exception("Yaml file does not match data coordinator/cloud configuration/app configuration")


def separate_cluster_files(all_template_list):
    cloud_parameters=[]#list of yaml dicts
    template_list = []
    for i in range(len(all_template_list)): 
        stream = open(all_template_list[i],'r')
        #Add system variables in yaml file and extract to dict
        stream_conv = stream.read() % os.environ
        stream_conv = yaml.load(stream_conv)
        if check_yaml_type(stream_conv)=='cluster':
            cloud_parameters.append(stream_conv)
            template_list.append(all_template_list[i])
            print_log("\t\t" + all_template_list[i])
    return cloud_parameters, template_list


def find_dcn_file(template_list):
    removed=0
    cloud_parameters=[]
    for i in range(len(template_list)): 
        stream = open(template_list[i-removed],'r')
        #Add system variables in yaml file and extract to dict
        stream_conv = stream.read() % os.environ
        stream_conv = yaml.load(stream_conv)
        stream.close()
        if check_yaml_type(stream_conv)=='dcn':
            cloud_parameters.append(stream_conv)
        else:
            del template_list[i-removed]
            removed+=1
    try:
        return template_list[0]
    except:
        Exception("No valid Data Coordinator configuration file specified (*.yaml)")


def dcn_extract(dcn_location):
    stream = open(dcn_location,'r')
    stream_conv = yaml.load(stream.read() % os.environ)
    stream.close()
    dcn_config=stream_conv

    return dcn_config

def set_system_variables(cloud_parameters,template_path,command=None):
    """
    Set up system variables based on cloud parameters from a yaml file
    NOTE: Each yaml file may overwrite system variables of another file
    """
    cloud_param = str(cloud_parameters["cloud"]["cloud_param"])
    file_path = os.environ['CLOUD_CONFIG']
    file_split = os.path.split(template_path)
    bash_file = open(os.environ["HOME"] + "/.bash_frieda",'w+')
    #print cloud_parameters["cloud"]["cloud_param"]
    stream = open(file_path,'r')
    stream_conv = yaml.load(stream.read() % os.environ)
    """universal variable set"""
    """platform specific variable set"""
    platform=cloud_parameters['cloud']['platform']
    if platform == "openstack":
        os.environ["OS_PASSWORD"]=stream_conv[cloud_param]["OS_PASSWORD"]
        os.environ["OS_AUTH_URL"]=stream_conv[cloud_param]["OS_AUTH_URL"]
        os.environ["OS_USERNAME"]=stream_conv[cloud_param]["OS_USERNAME"]
        os.environ["OS_TENANT_NAME"]=stream_conv[cloud_param]["OS_TENANT_NAME"]
        os.environ["OS_KEYPAIR"]=stream_conv[cloud_param]["keypair"]
        os.environ["EC2_KEYPAIR"]=stream_conv[cloud_param]["keypair"]
        os.environ["OS_SECURITY_GROUP"]=stream_conv[cloud_param]["security_group"]
        if command == "config":
            bash_file.write('export OS_PASSWORD='+stream_conv[cloud_param]["OS_PASSWORD"]+'\n')
            bash_file.write('export OS_AUTH_URL='+stream_conv[cloud_param]["OS_AUTH_URL"]+'\n')
            bash_file.write('export OS_USERNAME='+stream_conv[cloud_param]["OS_USERNAME"]+'\n')
            bash_file.write('export OS_TENANT_NAME='+stream_conv[cloud_param]["OS_TENANT_NAME"]+'\n')
            print_log("\t\tUse [ source ~/.bash_frieda ] to setup cloud variables")
    elif platform == "ec2":
        os.environ["AWS_ACCESS_KEY"]=stream_conv[cloud_param]["AWS_ACCESS_KEY"]
        os.environ["AWS_SECRET_KEY"]=stream_conv[cloud_param]["AWS_SECRET_KEY"]
        os.environ["EC2_ACCESS_KEY"]=stream_conv[cloud_param]["EC2_ACCESS_KEY"]
        os.environ["EC2_SECRET_KEY"]=stream_conv[cloud_param]["EC2_SECRET_KEY"]
        os.environ["EC2_KEYPAIR"]=stream_conv[cloud_param]["keypair"]
        os.environ["EC2_URL"]=stream_conv[cloud_param]["EC2_URL"]
        os.environ["EC2_SECURITY_GROUP"]=stream_conv[cloud_param]["security_group"]
        if command == "config":
            bash_file.write('export AWS_ACCESS_KEY='+stream_conv[cloud_param]["AWS_ACCESS_KEY"]+'\n')
            bash_file.write('export AWS_SECRET_KEY='+stream_conv[cloud_param]["AWS_SECRET_KEY"]+'\n')
            bash_file.write('export EC2_ACCESS_KEY='+stream_conv[cloud_param]["EC2_ACCESS_KEY"]+'\n')
            bash_file.write('export EC2_SECRET_KEY='+stream_conv[cloud_param]["EC2_SECRET_KEY"]+'\n')
            bash_file.write('export EC2_URL='+stream_conv["variables"]["EC2_URL"]+'\n')
            print_log("\t\tUse [ source ~/.bash_frieda ] to setup cloud variables")
    elif platform == "ip_address":
        os.environ["EC2_KEYPAIR"]=stream_conv[cloud_param]["keypair"]
    bash_file.close()
    return True
"""
#####################################
MAIN TASK FUNCTIONS
#####################################
"""

def start_old(args):
    subparser_name = args.subparser_name
    """
    get cloud provider will fail most times
    There is no longer a system variable for:
        "CLOUD_PROVIDER",
        "CLOUD_REGION",
        "CLOUD_AVAILABILITY_ZONE"
    """
    cloud_provider = get_cloud_provider()
    cloud_region = get_cloud_region()
    cloud_az = get_cloud_availability_zone()

    try:
        if subparser_name == "provision_instances":
            provision_instances(number_of_instances=args.number_of_instances,
                                provider=cloud_provider,
                                region=cloud_region,
                                keyname=args.keyname,
                                availability_zone=cloud_az,
                                spot_price=args.spot_price,
                                security_group=args.sec_group,
                                is_spot=args.is_spot)
        elif subparser_name == "run_app":
            run_application(gb_per_job=args.gb_per_job,
                            execution_command=args.exec_command,
                            file_prefix=args.file_prefix,
                            file_suffix=args.file_suffix,
                            provider=cloud_provider,
                            input=args.input,
                            output=args.output,
                            partitioning=args.partitioning,
                            region=cloud_region,
                            keyname=args.keyname,
                            ssh_key=args.ssh_key)
        elif subparser_name == "cleanup":
            cleanup_nodes(provider=cloud_provider,
                          region=cloud_region,
                          keyname=args.keyname,
                          ssh_key=args.ssh_key)
        elif subparser_name == "list_instances":
            list_instances(provider=cloud_provider,
                           region=cloud_region,
                           keyname=args.keyname)
        elif subparser_name == "terminate_instances":
            terminate_instances(provider=cloud_provider,
                                region=cloud_region,
                                keyname=args.keyname)
        elif subparser_name == "create_volume":
            create_volume(provider=cloud_provider,
                          region=cloud_region,
                          zone=cloud_az,
                          keyname=args.keyname,
                          ssh_key=args.ssh_key,
                          size=args.size)
        elif subparser_name == "upload_app":
            upload_app(tar_archive=args.tar_archive,
                       ssh_key=args.ssh_key,
                       keyname=args.keyname,
                       bexec=args.bexec,
                       provider=cloud_provider,
                       region=cloud_region)
        elif subparser_name == "upload_data":
            upload_data(source=args.source,
                        ssh_key=args.ssh_key,
                        keyname=args.keyname,
                        provider=cloud_provider,
                        region=cloud_region)
    except Exception:
        logger.error(traceback.format_exc())
def test_install(args):
    return 0

def config_run(args):
    #template_list,cloud_parameters=load_templates(args)
    print_log("Configuring your environment...")
    #if len(template_list) > 1:
        #print_log("\tNOTE: Configuration is not built for more than one cloud deployment")
    #   Extract system variables
    #for i in range(len(cloud_parameters)):
    #    set_system_variables(cloud_parameters[i],template_list[i])
    print_log("\tSetting CLOUD_CONFIG and FRIEDA_LOCAL_PATH")
    #Running config from frieda_wrapper.py directory configures these in the bash file
    frieda_local=str(os.environ['PWD'])+"/../../"
    cloud_loc=str(os.environ['PWD'])+"/tutorial/cloud.yaml"
    bash_file=open(str(os.environ['HOME'])+"/.bashrc",'a')
    try:
        os.environ['CLOUD_CONFIG']
        print_log("\tCLOUD_CONFIG exists as: "+str(os.environ['CLOUD_CONFIG']))
        proceed = raw_input("Make:"+cloud_loc+" the new location (y) or (n): ")
        if proceed=='n' or proceed=='N'or proceed=='No' or proceed=='no':
            pass
        else:
            bash_file.write("\nexport CLOUD_CONFIG="+cloud_loc+'\n')
    except:
        print_log("\t"+"CLOUD_CONFIG set as: "+cloud_loc)
        bash_file.write("\nexport CLOUD_CONFIG="+cloud_loc+'\n')
    try:
        os.environ['FRIEDA_LOCAL_PATH']
        print_log("\tFRIEDA_LOCAL_PATH exists as: "+str(os.environ['FRIEDA_LOCAL_PATH']))
        proceed = raw_input("Make:"+frieda_local+" the new location (y) or (n): ")
        if proceed=='n' or proceed=='N'or proceed=='No' or proceed=='no':
            pass
        else:
            bash_file.write("\nexport FRIEDA_LOCAL_PATH="+frieda_local+'\n')
    except:
        print_log("\t"+"FRIEDA_LOCAL_PATH"+frieda_local)
        bash_file.write("\nexport FRIEDA_LOCAL_PATH="+frieda_local+'\n')
    print_log("TO ENSURE SYSTEM CHANGES PLEASE RUN 'source ~/.bashrc'")
    bash_file.close()
    print_log("\tNOTE: System variables used in configuration files are not set here")
    #   Set local variables
    print_log("Configuration Complete")

def test_config(args):
    print_log("Checking Local Configuration Variables...")
    #   Check system var match/exist with YAML
    print_log("Local Configuration Check Complete")
    """
    Add coherence checks between files
    """
    print_log("Testing connection...")
    template_list,cloud_parameters=load_templates(args)
    #for i in range(len(cloud_parameters)):
    #    set_system_variables(cloud_parameters[i],template_list[i])
    test_ssh(cloud_parameters,template_list)
    #   initiate ssh test with config
    print_log("Connection Test Complete")
    #print_log("Checking instance IDs")
    """
    make sure instance IDs listed are available on node 
        and not repeated across yaml files (incorporate in ssh test?)
    """
    #print_log("Instance IDs are correct")

def dcn_run(args):
    partition_phase(args)
    frieda_run(args)

def frieda_run(args):
    template_list,cloud_parameters=load_templates(args)
    print_log("Initiating frieda master/worker instances...")
    #   each frieda all sets system variables and initiates

    for i in range(len(cloud_parameters)):
        set_system_variables(cloud_parameters[i],template_list[i])
        extract_ids(cloud_parameters[i])
        sys.argv=['frieda',
            'all',
            '-c', str(template_list[i]),
            '-i', str(cloud_parameters[i]['cloud']['instance_id'])
            ]
        print_log("\tDEBUG:::Using ID's:" + 
            str(cloud_parameters[i]['cloud']['instance_id']))
        time.sleep(3)#remove sleep and ID verification later
        print_log("\n\n\t\tBEGIN:::FRIEDA ALL OUTPUT")
        print "FOR TESTING ONLY::: CHANGE THIS LATER"
        frieda.main()
        #subprocess.Popen(sys.argv)
        #print_log("\t\tEND:::DATA COORDINATOR EXECUTE OUTPUT\n\n")
    #print_log("\tAll frieda instances initiated")

def staged_run(args):
    template_list,cloud_parameters=load_templates(args)
    #find some way to determine current stage
    #{(provision,master,workers,stop,all,scaleup) (default:stop)}
    current_stage = 'provision'
    up_to_stage = 'worker'
    for i in range(len(cloud_parameters)):
        set_system_variables(cloud_parameters[i],template_list[i])
        sys.argv=['frieda',
            'staged',
            '-s',str(args.stage),#STAGE (default: stop)
            '-c',str(template_list[i]),#CM_CONFIG (default: None)
            '-i',str(cloud_parameters[i]['cloud']['instance_id']),#INSTANCE_NAMES (default: None)
            #'-a','',#STORAGE_APP_CONFIG (default: None)
            #'-l',,#LOGS (default: logs)
            #'-o','',#OUTPUT (default: output)
            #'-t','',#STATE (default: state)
            #'-r','',#RESOURCE CONFIG (default:None)
            ]
        print_log("\n\n\t\tBEGIN:::STAGED OUTPUT for "+str())    
        subprocess.Popen(sys.argv)
def clean_all(args):
    """
    Plenty of room for improvement, currently trashes associated
        -yaml.tmp files
        -applaunch.yaml files
        -no files
        -appenv files
    """
    template_list,cloud_parameters=load_templates(args)
    print_log("\tremoving the following...")
    f=0
    for i in template_list:
        rem_file=os.path.split(i)[0]+'/applaunch.yaml'
        if os.path.isfile(rem_file):
            print_log("\t\t"+rem_file)
            os.remove(rem_file)
            f+=1
        rem_file=i+'.tmp'
        if os.path.isfile(rem_file):
            print_log("\t\t"+rem_file)
            os.remove(rem_file)
            f+=1
        rem_file=os.path.split(i)[0]+"/appenv"
        if os.path.isfile(rem_file):
            print_log("\t\t"+rem_file)
            os.remove(rem_file)
            f+=1
    if os.path.isfile('no'):
        print_log("\t\tno")
        os.remove('no')
        f+=1
    print_log("\tfiles removed: "+str(f))
def launch_nodes(args):
    """
    Create nodes
        attempt with 
            -OpenStack
            -EC2
        Automatically fill instance_id: {}
        Return IP:ID info
    """
    cloud_param=args
    # Authentication information so you can authenticate to DreamCompute
    # copy the details from the OpenStack RC file
    # https://dashboard.dreamcompute.com/project/access_and_security/api_access/openrc/ 

    auth_username = 'wfox'
    auth_password = 'lsh59$Nph'
    project_name = 'FG-298'
    auth_url = 'https://openstack.tacc.chameleoncloud.org:5000/v2.0/tokens'
    #region_name = 'RegionOne'   

    provider = get_driver(Provider.OPENSTACK)
    conn = provider(auth_username,
    auth_password,
    ex_force_auth_url=auth_url,
    ex_force_auth_version='2.0_password',
    ex_tenant_name=project_name)
    #ex_force_service_region=region_name)    

    # Get the image that we want to use by its id
    # NOTE: the image_id may change. See the documentation to find
    # all the images available to your user
    image_id = 'fc507be9-41e4-4c2c-b142-5c4256f8a814'
    image = conn.get_image(image_id)    

    # Get the flavor that we want to use by its id
    #flavor_id = '482da4376156195d3f5a4a4628df7bf9'
    #flavor = conn.ex_get_size(flavor_id)    
    instance = conn.create_node(name='PracticeInstance', image=image)#, size=flavor)
    pass
def end_nodes(args):
    pass

def all_mode(args):
    pass
"""
#####################################
PARSER FUNCTIONS
#####################################
"""
def setup_parser():
    parser = argparse.ArgumentParser(description="A command line interface to configure and execute experiments with FRIEDA on private and public clouds",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     epilog="A Lawrence Berkeley National Laboratory Software")
    parser.add_argument('--version', action='version', version='%(prog)s 1.1')
    parser.add_argument("--keyname", type=str, metavar="keyname", #default=get_ec2_keypair(),
                        help="the security keyname")
    subparsers = parser.add_subparsers(dest="subparser_name")
    """New Parsers"""
    _addConfigParser(subparsers)
    _addTestParser(subparsers)
    _addNewRunParser(subparsers)
    _addDcnParser(subparsers)
    _addStagedParser(subparsers)
    _addCleanParser(subparsers)
    _addLaunchNodesParser(subparsers)
    _addEndNodesParser(subparsers)
    _addAllModeParser(subparsers)
    """Tested and Updated Previous Parsers"""

    """Untested Deprecated Parsers"""
    _addProvisionParser(subparsers)
    _addListParser(subparsers)
    _addTerminateParser(subparsers)
    _addVolumeParser(subparsers)
    _addUploadParser(subparsers)
    _addCleanupParser(subparsers)
    _addRunParser(subparsers)
    _addUploadParser(subparsers)
    return parser.parse_args()
def _addProvisionParser(subparsers):
    provision_parser = subparsers.add_parser("provision_instances", help="provisions instances",
                                             formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    provision_parser.set_defaults(func=start_old)
    provision_parser.add_argument("number_of_instances", type=int, metavar="number_of_instances",
                                  help="the number of instances")
    provision_parser.add_argument("--spot-price", help="the desired spot price", metavar="price", default="0.1")
    provision_parser.add_argument("--sec-group", type=str, help="the security group", metavar="sec-group",
                                  default="default")
    provision_parser.add_argument("--is-spot", action="store_true", help="force spot instance provisioning")

def _addListParser(subparsers):
    list_parser = subparsers.add_parser("list_instances", help="lists running instances",
                                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    list_parser.set_defaults(func=start_old)
    
def _addTerminateParser(subparsers):
    terminate_parser = subparsers.add_parser("terminate_instances", help="terminates running instances")
    terminate_parser.set_defaults(func=start_old)

def _addVolumeParser(subparsers):
    volume_parser = subparsers.add_parser("create_volume", help="creates volumes",
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    volume_parser.set_defaults(func=start_old)
    volume_parser.add_argument("size", type=int, metavar="size", help="the size of the volume (= GB)")
    volume_parser.add_argument("--ssh_key", metavar="ssh-key", type=str, default=get_default_ssh_key(),
                               help="The private private ssh key")

def _addUploadParser(subparsers):
    upload_parser = subparsers.add_parser("upload_data", help="uploads data",
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    upload_parser.set_defaults(func=start_old)
    upload_parser.add_argument("source", metavar="source", type=str, help="the source directory")
    upload_parser.add_argument("--ssh_key", metavar="ssh-key", type=str, default=get_default_ssh_key(),
                               help="The private ssh key")

def _addCleanupParser(subparsers):
    cleanup_parser = subparsers.add_parser("cleanup", help="cleanup after application",
                                       formatter_class=argparse.ArgumentDefaultsHelpFormatter)

def _addRunParser(subparsers):
    run_parser = subparsers.add_parser("run_app", help="runs application",
                                       formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    run_parser.set_defaults(func=start_old)
    run_parser.add_argument("input", type=str, help="the data input directory. ")
    run_parser.add_argument("output", type=str, help="the data output directory. "
                                                     "This can be a local directory (FILE_PATH) or include a host name "
                                                     "(HOST:FILE_PATH) and  (USER@HOST:FILE_PATH). Please "
                                                     " remember that the HOST should authorize the id_rsa_frieda public key.")
    run_parser.add_argument("exec_command", type=str, help="the command to execute")
    run_parser.add_argument("file_prefix", type=str, help="the file prefix")
    run_parser.add_argument("file_suffix", type=str, help="the file suffix")
    run_parser.add_argument("--gb_per_job", metavar="gb-per-job", default=10000, type=int,
                            help="amount of data (= GB) to use for the job")
    run_parser.add_argument("--ssh_key", metavar="ssh-key", type=str, default=get_default_ssh_key(),
                            help="The ec2 private ssh key")
    run_parser.add_argument("--partitioning", metavar="partitioning", type=str, choices=DATA_PARTITIONING_MODES,
                            default="none", help="The data partitioning mode")

def _addUploadAppParser(subparsers):
    upload_app_parser = subparsers.add_parser("upload_app", help="uploads an application",
                                              formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    upload_app_parser.set_defaults(func=start_old)
    upload_app_parser.add_argument("tar_archive", type=str, help="the tar.gz archive")
    upload_app_parser.add_argument("--bexec", type=str, help="the build code command")
    upload_app_parser.add_argument("--ssh_key", type=str, default=get_default_ssh_key(), help="the ssh key")

def _addTestParser(subparsers):
    test_configuration = subparsers.add_parser("test", help="test each cloud configuration for connection",
                                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    test_configuration.set_defaults(func=test_config)
    test_configuration.add_argument('-t',"--templates", type=str,metavar="template_list",help="comma separate location of template (yaml) files")
    
def _addDcnParser(subparsers):
    run_dcn_configuration = subparsers.add_parser("run_dcn", help="run full frieda data coordinator execution",
                                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    run_dcn_configuration.set_defaults(func=dcn_run)
    run_dcn_configuration.add_argument('-t',"--templates", type=str,metavar="template_list",help="comma separate location of template (yaml) files")

def _addConfigParser(subparsers):
    config_parser = subparsers.add_parser("config", help="configure instances based on yaml files and system variables",
                                           formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    config_parser.set_defaults(func=config_run)
    config_parser.add_argument('-t',"--templates", type=str,metavar="template_list",help="comma separate location of template (yaml) files")

def _addNewRunParser(subparsers):
    run_configuration = subparsers.add_parser("run", help="run full frieda execution",
                                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    run_configuration.set_defaults(func=frieda_run)
    run_configuration.add_argument('-t',"--templates", type=str,metavar="template_list",help="comma separate location of template (yaml) files")

def _addStagedParser(subparsers):
    staged_configuration = subparsers.add_parser("staged", help="run frieda data coordinator up through specified stage",
                                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    staged_configuration.set_defaults(func=staged_run)
    staged_configuration.add_argument('-t',"--templates", type=str,metavar="template_list",help="comma separate location of template (yaml) files")
    staged_configuration.add_argument('-d',"--dcn",metavar="dcn_location",help="Data Coordinator configuration location")
    staged_configuration.add_argument('-s',"--stage",metavar="stage_to_run",help="enter the stage in which to tun to:\n (provision,master,workers,stop,all,scaleup)")
    staged_configuration.add_argument('-c',"--current",metavar="state_directory",help="directory state is stored for staged run")

def _addCleanParser(subparsers):
    test_configuration = subparsers.add_parser("clean", help="clean all temp and appenv files",
                                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    test_configuration.set_defaults(func=clean_all)
    test_configuration.add_argument('-t',"--templates", type=str,metavar="template_list",help="comma separate location of template (yaml) files")

def _addLaunchNodesParser(subparsers):
    provision_configuration = subparsers.add_parser("launch", help="Launch nodes and add them to the corresponding yaml file.  ",
                                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    provision_configuration.set_defaults(func=launch_nodes)
    provision_configuration.add_argument('-t',"--templates", type=str,metavar="template_list",help="comma separate location of template (yaml) files")
def _addEndNodesParser(subparsers):
    provision_end_nodes = subparsers.add_parser("end", help="Terminate nodes if they exist.  ",
                                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    provision_end_nodes.set_defaults(func=end_nodes)
    provision_end_nodes.add_argument('-t',"--templates", type=str,metavar="template_list",help="comma separate location of template (yaml) files")

def _addAllModeParser(subparsers):
    provision_all_mode = subparsers.add_parser("all", help="Launch Nodes / Provision Data/ Execute Data Coordinator or FRIEDA only/ End dcn daemon/ Retrieve data/ give option to delete nodes.  ",
                                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    provision_all_mode.set_defaults(func=all_mode)
    provision_all_mode.add_argument('-t',"--templates", type=str,metavar="template_list",help="comma separate location of template (yaml) files")

if __name__ == "__main__":
    args = setup_parser()
    print_log("\n\nMODE INITIATED: " + args.subparser_name)
    args.func(args)
    print_log("FINISHED")


