'''
A variation of vector clock, specially designed for StateManager for FREIDA.
Author: Tonglin Li
Email: tonglinli@lbl.gov, leo.entropy@gmail.com
'''
import time

class VectorClock:
	hostType = ''
	masterClock = 0
	localClock = 0
	separator= ' '

	def __init__(self, separator):
		self.masterClock = 0
		self.localClock = 0
		self.separator=separator

	def updateLocalClock(self):
		self.localClock = int(round(1000*1000*time.time()))
	def updateMasterClockByLocal(self):
		self.masterClock = int(round(1000*1000*time.time())) #unit in microsecond
	def updateMasterClockByValue(self, masterClock):
		self.masterClock = masterClock

	def nowMasterClock(self):
		self.masterClock = int(round(1000*1000*time.time()))
		return self.separator+str(self.masterClock)	
#global
# tell if A is later than B
def laterThan(clockA, clockB):
	if clockA.masterClock > clockB.masterClock:
		print 1
		return True
	elif clockA.masterClock < clockB.masterClock:
		print 2
		return False
	elif clockA.masterClock == clockB.masterClock:
		if clockA.localClock > clockB.localClock:
			print 3
			return True
		else:
			print 4
			return False 

