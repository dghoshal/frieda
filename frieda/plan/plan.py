#!/usr/bin/python

"""This module calculates the storage requirements for user defined application requirements."""

__author__="pmantha"
__date__ ="$Dec 19, 2012 2:37:53 PM$"

FAILURE=1
SUCCESS=0

import frieda.storage as storage
from urlparse import urlparse
import argparse
import logging
import yaml
import sys
import os
import pdb  
import math

logging.basicConfig(level=logging.INFO)

def parse_args():
    """ parse input parameters of the file """

    parser = argparse.ArgumentParser()
    
    parser.add_argument("-s", "--spreqfile", dest="spreqfile",
                      help="Application storage plan request", required=True)
    parser.add_argument("-r", "--resourcefile", dest="resourcefile",
                      help="Resource configuration of the infrastructure", required=True)
    parser.add_argument("-c", "--cmconfigfile", dest="cmconfigfile",
                      help=" cloud monkey configuration", required=True)

    args = vars(parser.parse_args())

    return args

def get_plan(plan_request, resource_file):
    """ If successful, returns storage plan for the given application request
     
    param options - provides input parameters passed to the file. 
    
    """

    pdb.set_trace()

    request = storage.StoragePlanRequest(plan_request, resource_file)
    logging.info("Application Request : " + str(request))
    # submit request to provision calculator
    plan = storage.StoragePlan(request)

    k = plan.generate_plan()
    
    if k == SUCCESS:
        return plan
    else:
        logging.info("Storage algorithm failed with error " + plan.error_message )
        sys.exit()

def load_irmo(irmofile, plan):
    """ Integrates the plan details with input imrofile. 
    
    param irmofile - string containing the input irmo filename
    param plan - storage plan object contains all the plan master 
                 & worker details

    returns  updated irmo details                 
    
    """

    fh = open(irmofile, 'r')    
    irmoStr= fh.read() % os.environ
    fh.close()    

    irmoDict=yaml.load(irmoStr)
    logging.debug("IRMO contents: " + str(irmoDict))
    
    # Populate storage  for input only for realtime
    # for pre-partition bypass it.
    if plan.frieda['mode'] == "realtime":
        storage_irmo_dict = populate_master_storage(irmoDict, plan.master)
    
    # separate provisioning for log storage is required, 
    # if input data is in existing volume-id.
    if plan.master.get('input',None) == None or plan.master['data_placement']['input'].get('volume_id', None) != None:
        storage_irmo_dict = populate_master_log_storage(irmoDict, plan.master)
    
    storage_irmo_dict = populate_worker_storage(storage_irmo_dict, plan.worker)
    
    storage_irmo_dict = populate_cloud_details(storage_irmo_dict, plan)
    
    storage_irmo_dict = populate_preparation_details(storage_irmo_dict, plan)
    
    return storage_irmo_dict

def populate_cloud_details(irmoDict, plan):
    """ populate VM details in the cloud section of irmofile
    
    param irmoDict - Dictionary containing all the input irmo file details.
    param plan - storage plan object contains all the plan master 
                 & worker details

    returns  updated irmo details
    
    """
    
    irmoDict['cloud']['image_id']=plan.vm['image_id']
    irmoDict['cloud']['instance_type']=plan.vm['vm_type']    
    irmoDict['cloud']['ssh_user']=plan.vm['ssh_user']
    return irmoDict
    
def populate_preparation_details(irmoDict, plan):
    """ Populate preparation and  storage location details for input.
     
    param irmoDict - Dictionary containing all the input irmo file details.
    param plan - storage plan object contains all the plan master 
                 & worker details

    returns  updated irmo details    
    
    """
             
    if  plan.frieda['mode'] == 'realtime': 
        irmoDict['preparation']['input']={'params': {'input_root_dir':'', 'input_dir': ''}}
        if plan.master['data_placement']['input']['storage_choice'] == 'block':
            if plan.master['data_placement']['input'].get('volume_id', None) == None:
                irmoDict['role']['master']['actions'].append('input_mkfs')
                irmoDict['preparation']['input']['params']['input_root_dir'] = plan.vm['block']                                               
                irmoDict['preparation']['input']['params']['input_dir'] = plan.application['name'] + '_input'                                                            
            else:
                irmoDict['role']['master']['actions'].append('mount')
                irmoDict['preparation']['input']['params']['input_root_dir'] = os.path.dirname(plan.master['data_placement']['input']['volume_input_path'])
                irmoDict['preparation']['input']['params']['input_dir'] = os.path.basename(plan.master['data_placement']['input']['volume_input_path'])
        elif  plan.master['data_placement']['input']['storage_choice'] == 'local':            
            irmoDict['preparation']['input']['params']['input_root_dir'] = plan.vm['local'] 
        
        irmoDict['role']['master']['actions'].append('input')
        
    # Populate preparation and  storage location details for task data.
    irmoDict['preparation']['task_data']={'params': {'task_root_dir':'','task_dir':''}}        
    if plan.worker['data_placement']['task_data']['storage_choice'] == "local":
        irmoDict['preparation']['task_data']={'params': {'task_root_dir':{}}}
        irmoDict['preparation']['task_data']['params']['task_root_dir'] = plan.vm['local']
    elif plan.worker['data_placement']['task_data']['storage_choice'] == "block_nfs":
        irmoDict['role']['worker']['actions'].append('mount_nfs')
        irmoDict['preparation']['task_data']['params']['task_root_dir'] = plan.vm['nfs']
    else:
        irmoDict['preparation']['task_data']['params']['task_root_dir'] = plan.vm['block']
    irmoDict['preparation']['task_data']['params']['task_dir'] = plan.application['name'] + '_task' 
    irmoDict['role']['worker']['actions'].append('task_data')

    # Populate preparation and  storage location details for output data.
    irmoDict['preparation']['output']={'params': {'output_root_dir':'','output_dir':''}}            
    if plan.output.get('storage_choice', None) != None:
        if plan.output['storage_choice'] == 'block':
            irmoDict['preparation']['output']['params']['output_root_dir'] = plan.vm['block']
            irmoDict['preparation']['output']['params']['output_dir'] = plan.application['name'] + '_output'    
            irmoDict['role']['worker']['actions'].append('output_data')
        elif plan.output['storage_choice'] == 'block_nfs':
            irmoDict['preparation']['output']['params']['output_root_dir'] = plan.vm['nfs']        
            irmoDict['preparation']['output']['params']['output_dir'] = plan.application['name'] + '_output'            
            irmoDict['role']['worker']['actions'].append('output')
        else:
            pass 

    # Populate preparation and  storage location details for application data.        
    irmoDict['preparation']['shared_data']={'params': {'shared_root_dir':'', 'shared_dir':''}}    
    if plan.worker.get('application_shared_data', None) != None:
        if plan.worker['application_shared_data']['storage_choice'] == 'block':
            irmoDict['preparation']['shared_data']['params']['shared_root_dir'] = plan.vm['block']
        elif plan.worker['application_shared_data']['storage_choice']  == 'block_nfs':
            irmoDict['preparation']['shared_data']['params']['shared_root_dir'] = plan.vm['nfs']        
        else:
            irmoDict['preparation']['shared_data']['params']['shared_root_dir'] = plan.vm['local']
        irmoDict['preparation']['shared_data']['params']['shared_dir'] = plan.application['name'] + '_shared_data'
        irmoDict['role']['worker']['actions'].append('shared_data')

    # Modify execution master commands with input_data. 
    inputdir = os.path.join(irmoDict['preparation']['input']['params']['input_root_dir'],irmoDict['preparation']['input']['params']['input_dir'])
    irmoDict['execution']['master']['commands'] = [ i.replace("<input_data>",inputdir) for i in irmoDict['execution']['master']['commands']] 


    # Populate contextualization and  storage location details for log data for both master and worker.
    
    ## worker details
    irmoDict['preparation']['worker_log_data']={'params': {'log_root_dir':'','log_dir':''}} 
    if plan.worker['data_placement']['log']['storage_choice'] == "local":
        irmoDict['preparation']['worker_log_data']['params']['log_root_dir'] = plan.vm['local']
    elif plan.worker['data_placement']['log']['storage_choice'] == "block_nfs":
        irmoDict['preparation']['worker']['actions'].append('mount_nfs')
        irmoDict['preparation']['worker_log_data']['params']['log_root_dir'] = plan.vm['nfs']
    else:
        irmoDict['preparation']['worker_log_data']['params']['log_root_dir'] = plan.vm['block']
    irmoDict['preparation']['worker_log_data']['params']['log_dir'] = plan.application['name'] + '_log' 
    irmoDict['role']['worker']['actions'].append('worker_log_data')
    
    ## master details
    irmoDict['preparation']['master_log_data']={'params': {'log_root_dir':'','log_dir':''}}     
    if plan.master['data_placement']['log']['storage_choice'] == "local":
        irmoDict['preparation']['master_log_data']['params']['log_root_dir'] = plan.vm['local']
    else:
        irmoDict['preparation']['master_log_data']['params']['log_root_dir'] = plan.vm['block']
    irmoDict['preparation']['master_log_data']['params']['log_dir'] = plan.application['name'] + '_log' 
    irmoDict['role']['master']['actions'].append('master_log_data')
        
    return irmoDict                           
    
    
def populate_master_log_storage(irmoDict,master):
    """ populate master log details in the role and provision section of irmofile
     
    param irmoDict - Dictionary containing all the input irmo file details.
    param master -  Dictionary containing storage plan details of master

    returns  updated irmo details    
    
    """

    if irmoDict['role'].get('master',None) != None:
        if master['data_placement']['log']['storage_choice'] == 'block':
            irmoDict['role']['master']['storage'].append('log_data')
            irmoDict['provision']['storage']['log_data'] = {}
            irmoDict['provision']['storage']['log_data']['type'] = 'block'
            irmoDict['provision']['storage']['log_data']['size'] = convert_to_GB(master['storage_setup']['block_storage_size'])
            # add mkfs to the master context
            mkfsindex = irmoDict['role']['master']['actions'].index('mount') + 1
            irmoDict['role']['master']['actions'].insert(mkfsindex,'log_mkfs')
    return irmoDict        
        

def populate_master_storage(irmoDict, master):
    """  populate master details in the role and provision section of irmofile
     
    param irmoDict - Dictionary containing all the input irmo file details.
    param master -  Dictionary containing storage plan details of master

    returns  updated irmo details    
    
    """
    
    if irmoDict['role'].get('master',None) != None:
        if master['data_placement']['input']['storage_choice'] == 'block':
            irmoDict['role']['master']['storage']= ['application_data']
            if irmoDict.get('provision', None) == None:
                irmoDict['provision'] = {}
                irmoDict['provision']['storage'] = {}
                
            irmoDict['provision']['storage']['application_data'] = {}
            irmoDict['provision']['storage']['application_data']['type'] = 'block'
            if master['data_placement']['input'].get('volume_id', None) != None:
                irmoDict['provision']['storage']['application_data']['id'] = master['data_placement']['input']['volume_id']
            else:                
                irmoDict['provision']['storage']['application_data']['size'] = convert_to_GB(master['storage_setup']['block_storage_size'])

                # add mkfs to the master context
                try:
                    mkfsindex = irmoDict['role']['worker']['actions'].index('mount') + 1
                    irmoDict['role']['worker']['actions'].insert(mkfsindex,'mkfs')
                except:            
                    irmoDict['role']['worker']['actions'].insert(1,'mount')
                    irmoDict['role']['worker']['actions'].insert(2,'mkfs')
    return irmoDict

            
            
def populate_worker_storage(irmoDict, worker):
    """  populate worker details in the role section of irmofile
     
    param irmoDict - Dictionary containing all the input irmo file details.
    param worker -  Dictionary containing storage plan details of worker

    returns  updated irmo details    
    
    """
    
    # 
    if irmoDict['role'].get('worker', None) != None:
            irmoDict['role']['worker']['storage']= ['output']
            
            if irmoDict.get('provision', None) == None:
                irmoDict['provision'] = {}
                irmoDict['provision']['storage'] = {}            
                
            irmoDict['provision']['storage']['output'] = {}
            
            if worker['storage_setup']['block_storage_size'] > 0:
                irmoDict['provision']['storage']['output']['type'] = 'block'
                irmoDict['provision']['storage']['output']['size'] = convert_to_GB(worker['storage_setup']['block_storage_size'])
            else:
                irmoDict['provision']['storage']['output']['type'] = 'block_nfs'
                irmoDict['provision']['storage']['output']['size'] = convert_to_GB(worker['storage_setup']['block_nfs_storage'])
            
            # add mkfs to the worker storage contexts
            try:
                mkfsindex = irmoDict['role']['worker']['actions'].index('mount') + 1
                irmoDict['role']['worker']['actions'].insert(mkfsindex,'mkfs')
            except:
                irmoDict['role']['worker']['actions'].insert(1,'mount')
                irmoDict['role']['worker']['actions'].insert(2,'mkfs')
            
    return irmoDict            
            

def convert_to_GB(storage_size_mb):
    """ Converts the given input storage_size request from MB to GB 
        
    param storage_size_mb - input storage size in MB.
    
    returns value in giga bytes.
    """
    
    return int(math.ceil(float(storage_size_mb)/1000))

def dump_storage_irmo(storage_irmo_dict, irmofile, newirmofile):
    """ Dump back the populated storage_irmo_dict to YAML file 
        
    param storage_irmo_dict - Dictionary with updated irmo details
    param irmofile - input irmo file name.
    param newirmofile - output irmo file name.
    
    """
    
    fh = open( newirmofile, 'w')           
    yaml.dump(storage_irmo_dict, fh, default_flow_style=False)        
    fh.close()
    
def populate_env_file(env_file, newirmofile, plan, storage_irmo_dict):
    """ Populate environment variables for next subsequent steps 
    
    param env_file : Name of the environment file to be generated
    
    """
    
    fh = open( env_file, 'w')
    fh.write("export INTEGRATION_FILE=%s\n" % newirmofile)

    instances = ""
    for i in range(plan.vm['count']):
        if i == 0:
            instances = "1"
        else:
            instances = instances + " " + str(i+1)
    
    fh.write("export INSTANCES=\"%s\"\n" % instances)
    
    input_path = os.path.join(storage_irmo_dict['preparation']['input']['params']['input_root_dir'],
                              storage_irmo_dict['preparation']['input']['params']['input_dir'])
    
    if "<" in input_path:
        """ input data path is not provided by user """
        pass
    else:
        fh.write("export INPUT_DIR=%s\n" % input_path )

    fh.write("export FRIEDA_MODE=%s\n" % plan.frieda['mode'] )

    fh.close()    
    logging.info("source the environment file as below \n source " + env_file )
    
    pass


def generate_storage_plan_cm_config(storage_app_request, resourcefile, cmconfig ):

    # Submit application request and Get the storage Plan.
    plan = get_plan(storage_app_request, resourcefile)
        
    logging.info("Storage Plan : " + str(plan))

    # Modify cm configuration file to populate details obtained from storage plan
    storage_cm_dict = load_irmo(cmconfig, plan)

    # Dump irmo and storage plan into a new file
    spcmconfig = "%(FRIEDA_HOME)s/resources/configs/storage-" % (os.environ) + os.path.basename(cmconfig)    
    dump_storage_irmo(storage_cm_dict, cmconfig, spcmconfig) 

    # populate environment variables for subsequent steps.
    env_file = "%(FRIEDA_HOME)s/resources/configs/storage-" % (os.environ) + os.path.basename(cmconfig) + ".env" 
    populate_env_file(env_file, spcmconfig, plan, storage_cm_dict) 


    
    
def main():

    # parse arguments
    args = parse_args()
    
    # generate updated cloud monkey configuration with storage plan details
    generate_storage_plan_cm_config(args['spreqfile'], args['resourcefile'],args['cmconfigfile'] )
    
    
                
if __name__ == "__main__":
    main()
