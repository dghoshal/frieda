"""This module calculates the storage and compute resource requirements for user defined application requirements."""

__author__="pmantha"
__date__ ="$Dec 19, 2012 2:37:53 PM$"


import math
from urlparse import urlparse
import yaml
import sys
import pdb
import argparse
import logging
logging.basicConfig(level=logging.INFO)
LOG = logging.getLogger(__name__)


FAILURE=1
SUCCESS=0



class spconfig:
    def __init__(self, spreqfile):
        stream = open(spreqfile, 'r')
        self.yamlDict = yaml.load(stream)

    def data(self):
        return self.yamlDict.get('data', None)

    def task(self):
        return self.yamlDict.get('task', None)

    def resource(self):
        return self.yamlDict.get('resource', None)

    def frieda(self):
        return self.yamlDict.get('frieda', None)

    def app_details(self):
        return self.yamlDict.get('application_details', None)



class StoragePlanRequest:
    def __init__(self, spreqfile, resourcefile): 
        """ Initialize request """
                
        self.spreq = spconfig(spreqfile)


        self.app_details = self.spreq.app_details()
        self.input = self.spreq.data().get('input',None)
        self.source = self.spreq.data().get('source',None)
        self.shared_data = self.spreq.data().get('shared_data',None)
        self.task = self.spreq.task()
        self.vm = self.spreq.resource()
        self.output = self.spreq.data()['output']
        self.frieda = self.spreq.frieda()
        

        # Loading reference data/storage configuration
        stream = open(resourcefile, 'r')
        storage_config = yaml.load(stream)
        
        self.block_nfs_limit = int(storage_config['BLOCK_NFS_LIMIT'])
        self.local_nfs_limit = int(storage_config['LOCAL_NFS_LIMIT'])
        self.master_controller_vm_count = int(storage_config['MASTER_CONTROLLER_VM_COUNT'])
        self.frieda_log_size = int(storage_config['FRIEDA_LOG_SIZE'])
                        
        self.compute_provisioning_resources = storage_config['RESOURCES'] 
        
        stream.close()





class StoragePlan:
    def __init__(self,provision_request):
        """ Initialize plan """

        self._prvsn_request = provision_request
        self._frieda = {}
        self._output = {}
        self._vm = {}
        self._worker = {}
        self._master = {}
        self.error_message = ""
        self._storage_plan = {}
        self._master_local_storage = 0
        self._master_block_storage = 0
        self._worker_local_storage = 0
        self._worker_block_storage = 0
        self._worker_block_nfs_storage = 0
        self._worker_local_nfs_storage = 0
        self.application_shared_data_storage_choice =''
        self.application_source_storage_choice=''
        self.task_data_storage_choice=''
        self.master_input_storage_choice =''
        self.worker_log_storage_choice =''
        self.worker_log_storage_choice =''
        self.application = {}
        
    def output():
        doc = "The output property."
        def fget(self):
            return self._output
        def fset(self, value):
            self._output = value
        def fdel(self):
            del self._output
        return locals()
    output = property(**output())


    def storage_plan():
        doc = "The storage_plan property."
        def fget(self):
            return self._storage_plan
        def fset(self, value):
            self._storage_plan = value
        def fdel(self):
            del self._storage_plan
        return locals()
    storage_plan = property(**storage_plan())


    def vm():
        doc = "The vm property."
        def fget(self):
            return self._vm
        def fset(self, value):
            self._vm = value
        def fdel(self):
            del self._vm
        return locals()
    vm = property(**vm())

    def worker():
        doc = "The worker property."
        def fget(self):
            return self._worker
        def fset(self, value):
            self._worker = value
        def fdel(self):
            del self._worker
        return locals()
    worker = property(**worker())

    def master():
        doc = "The master property."
        def fget(self):
            return self._master
        def fset(self, value):
            self._master = value
        def fdel(self):
            del self._master
        return locals()
    master = property(**master())

    def frieda():
        doc = "The frieda property."
        def fget(self):
            return self._frieda
        def fset(self, value):
            self._frieda = value
        def fdel(self):
            del self._frieda
        return locals()
    frieda = property(**frieda()) 
    

    def getVMDetailsFromCPS(self):
        """ Get details of the user input vm from the resource.yaml """

        for k,v in self._prvsn_request.compute_provisioning_resources.items():
            if v['vm_type'] == self._prvsn_request.vm['vm_type']:
                return v

    def provision_log(self):
        """ provision storage for log 
        
            if master VM can store a log size of self._prvsn_request.frieda_log_size on its 
            local disk; 
                provision local storage
            else 
                provision block storage.
        
            provision local/local+nfs/block/block+nfs storage choice for master based
            on the size of log data and file system limits.
        
        """
                    
        if self._master_local_storage + self._prvsn_request.frieda_log_size <= self.vm_details['instance_storage']:
            self._master_local_storage = self._master_local_storage + self._prvsn_request.frieda_log_size
            self.master_log_storage_choice = 'local'
        else:            
            self._master_block_storage = self._master_block_storage + self._prvsn_request.frieda_log_size     
            self.master_log_storage_choice = 'block'


        if self._prvsn_request.vm['count'] < self._prvsn_request.local_nfs_limit and \
           self._worker_local_nfs_storage + self._prvsn_request.frieda_log_size \
               < self.vm_details['instance_storage']:
            self._worker_local_nfs_storage = self._worker_local_nfs_storage + self._prvsn_request.frieda_log_size
            self.worker_log_storage_choice = 'local_nfs'                              
        
        elif self._worker_local_storage + self._prvsn_request.frieda_log_size  < self.vm_details['instance_storage'] and \
            self._prvsn_request.vm['count'] >= self._prvsn_request.local_nfs_limit:
            self._worker_local_storage = self._worker_local_storage + self._prvsn_request.frieda_log_size
            self.worker_log_storage_choice = 'local'            
        
        elif self._prvsn_request.vm['count'] < self._prvsn_request.block_nfs_limit:
            self._worker_block_nfs_storage = self._worker_block_nfs_storage + self._prvsn_request.frieda_log_size
            self.worker_log_storage_choice = 'block_nfs'            
        else:
            self._worker_block_storage = self._worker_block_storage + self._prvsn_request.frieda_log_size
            self.worker_log_storage_choice = 'block'
                                         
                
                
    def provision_input(self):
        """ provision storage for input data 
        
        # In case of real time - Input data needs to be with Master, 
            # so provision space for input data only for master.
            
        if data is already present on EBS volumes, 
            just pass the volume-id to the IRMO.
        else 
            provision space for input data; if input_data doesn't fit on local storage 
            of VM provision block storage 
        
        # In case of Pre-partition - Input is divided and equally stored
        # on Worker VMS, provision local/local+nfs/block/block+nfs based
        on the size of input data partition and file system limits.
        
        """
        

        if self.frieda['mode'] == "pre-partitioned":        
            self.worker['input'] = {}
            per_worker_input_data = self._prvsn_request.input['size'] / \
                                    self._prvsn_request.vm['count']
                                    
            if self._prvsn_request.vm['count'] < self._prvsn_request.local_nfs_limit and \
               self._worker_local_nfs_storage + self._prvsn_request.input['size'] \
                   < self.vm_details['instance_storage']:
                self._worker_local_nfs_storage = self._worker_local_nfs_storage + \
                     self._prvsn_request.input['size']
                self.worker_input_storage_choice = 'local_nfs'                              
            
            elif ( self._worker_local_storage + per_worker_input_data ) < self.vm_details['instance_storage'] and \
                self._prvsn_request.vm['count'] >= self._prvsn_request.local_nfs_limit:
            
                self._worker_local_storage = self._worker_local_storage + per_worker_input_data
                self.worker_input_storage_choice = 'local'            
            
            elif self._prvsn_request.vm['count'] < self._prvsn_request.block_nfs_limit:
                self._worker_block_nfs_storage = self._worker_block_nfs_storage + \
                    self._prvsn_request.input['size']
                self.worker_input_storage_choice = 'block_nfs'            
            else:
                self._worker_block_storage = self._worker_block_storage + per_worker_input_data
                self.worker_input_storage_choice = 'block'
        else:
            if  self._prvsn_request.input.get('volume_id',None) != None:
                # For master we consider only block storage
                self.master_input_storage_choice = 'block'
            else:
                if self._master_local_storage + self._prvsn_request.input['size'] <= self.vm_details['instance_storage']:
                    self.master_input_storage_choice = 'local'
                    self._master_local_storage = self._master_local_storage + self._prvsn_request.input['size']
                else:
                    self.master_input_storage_choice = 'block'
                    self._master_block_storage = self._master_block_storage + self._prvsn_request.input['size']


    def provision_output(self):
        """ provision storage for output data
        
            Output is either stored on object/ Block. 
            if output needs to be stored on Object for aggregate analysis of output data, 
                then provision object storage for all the tasks output data. 
            else if 
                user specified VM count <  self._prvsn_request.block_nfs_limit; provision block+nfs storage
                for all the tasks output data
            else
                leave tasks output on storage attached to - Useful if aggregate analysis 
                of output data is not required. 
                
            Consider that the tasks are almost divided equally among all VMS equally.. 
            If not, handled later. """
            
                
        if self._prvsn_request.output['to_object_or_block'] == "object":
            self.output['object_storage_size'] = self.leave_margin(self._prvsn_request.task['count'] * self._prvsn_request.task['output_data_size'])
            self.output['storage_choice'] = 'object'
        else:
            if self._prvsn_request.vm['count'] < self._prvsn_request.block_nfs_limit:
                self.output['storage_choice'] = 'block_nfs'
                self._worker_block_nfs_storage = self._worker_block_nfs_storage + (self._prvsn_request.task['count'] * self._prvsn_request.task['output_data_size'])
            else:
                self.output['storage_choice'] = 'block'
                # Provision block storage and leave output data of tasks on worker itself.
                
                self._worker_block_storage = self._worker_block_storage + int( math.ceil((float(self._prvsn_request.task['count'])/self._prvsn_request.vm['count'])) * \
                                             self._prvsn_request.task['output_data_size'])
        

    def provision_application_sources(self): 
        """ provision storage for application sources
            if application source information is given for provisioning,
                if the size(application source) < vm size 
                    if proposed vm count <= LOCAL_NFS_LIMIT:
                        place the application source on worker using storage choice local+nfs.
                    else 
                        place the application source on local disk                
                else 
                    if proposed vm count <= BLOCK_NFS_LIMIT
                        place the application source on worker using storage choice block+nfs
                    else 
                        place the application source on workers block storage;            
            """
        
        if  self._prvsn_request.source != None and \
            self._prvsn_request.source.get('size',0) != 0:
            
            
            if self._worker_local_nfs_storage + self._prvsn_request.source['size'] \
                <= self.vm_details['instance_storage']:
                
                if self._prvsn_request.vm['count'] < self._prvsn_request.local_nfs_limit:
                    self._worker_local_nfs_storage = self._worker_local_nfs_storage + \
                        self._prvsn_request.source['size']
                    self.application_source_storage_choice = 'local_nfs'
                else:
                    self._worker_local_storage = self._worker_local_storage + self._prvsn_request.source['size']
                    self.application_source_storage_choice = 'local'                    
            else:            
                if self._prvsn_request.vm['count'] < self._prvsn_request.block_nfs_limit:
                    self._worker_block_nfs_storage = self._worker_block_nfs_storage + self._prvsn_request.source['size']
                    self.application_source_storage_choice = 'block_nfs'
                else:
                    self._worker_block_storage = self._worker_block_storage + self._prvsn_request.source['size']
                    self.application_source_storage_choice = 'block' 
        

    def provision_application_shared_data(self): 
        """ provision storage for application sources

            if application shared data is given for provisioning,
                if the size(application shared data) < vm size 
                    if proposed vm count <= LOCAL_NFS_LIMIT:
                        place the application shared data on worker using storage choice local+nfs.
                    else if application shared fits on local disk
                        place the application source on local disk                
                else 
                    if proposed vm count <= BLOCK_NFS_LIMIT
                        place the application source on worker using storage choice block+nfs
                    else 
                        place the application source on workers block storage;
            
        """

        if  self._prvsn_request.shared_data != None and self._prvsn_request.shared_data.get('size',0) != 0:
            self.worker['application_shared_data']={}

            if self._worker_local_nfs_storage + self._prvsn_request.shared_data['size'] \
                <= self.vm_details['instance_storage']:                
                if self._prvsn_request.vm['count'] < self._prvsn_request.local_nfs_limit:
                    self._worker_local_nfs_storage = self._worker_local_nfs_storage + self._prvsn_request.shared_data['size']
                    self.application_source_storage_choice = 'local_nfs'            
                else:
                    self._worker_local_storage = self._worker_local_storage + self._prvsn_request.shared_data['size']
                    self.application_shared_data_storage_choice = 'local'
            else:
                if self._prvsn_request.vm['count'] < self._prvsn_request.block_nfs_limit:
                    self._worker_block_nfs_storage = self._worker_block_nfs_storage + self._prvsn_request.shared_data['size']
                    self.application_shared_data_storage_choice = 'block_nfs'
                else:
                    self._worker_block_storage = self._worker_block_storage + self._prvsn_request.shared_data['size']
                    self.application_shared_data_storage_choice = 'block'
                
                
    def provision_task(self):            
        """ provision storage for task data
        
            Provision local disk, if total task data size (per_vm_task_data), that can run
            concurrently on VM (tasks_per_vm)  else if the 
            proposed_vm_count < self._prvsn_request.block_nfs_limit, then provision block+nfs storage for 
            all the tasks (proposed_vm_count * tasks_per_vm) that are running currently.
            else provision block storage for tasks running on the VM (tasks_per_vm)        
        """


        per_task_data = self._prvsn_request.task['input_data_size'] + \
                        self._prvsn_request.task['temp_data_size'] + \
                        self._prvsn_request.task['output_data_size']
                        
        per_vm_task_data = self._prvsn_request.task['tasks_per_vm'] * per_task_data
        

        if self._prvsn_request.vm['count'] < self._prvsn_request.local_nfs_limit and \
           self._worker_local_nfs_storage + ( self._prvsn_request.vm['count'] * \
                per_vm_task_data ) < self.vm_details['instance_storage']:
            self._worker_local_nfs_storage = self._worker_local_nfs_storage + \
                ( self._prvsn_request.vm['count'] * per_vm_task_data )
            self.task_data_storage_choice = 'local_nfs'                              
            
        elif ( self._worker_local_storage + per_vm_task_data ) < self.vm_details['instance_storage'] and \
            self._prvsn_request.vm['count'] >= self._prvsn_request.local_nfs_limit:
            
            self._worker_local_storage = self._worker_local_storage + per_vm_task_data
            self.task_data_storage_choice = 'local'            
            
        elif self._prvsn_request.vm['count'] < self._prvsn_request.block_nfs_limit:
            self._worker_block_nfs_storage = self._worker_block_nfs_storage +\
                 (self._prvsn_request.vm['count'] * per_vm_task_data )
            self.task_data_storage_choice = 'block_nfs'            
        else:
            self._worker_block_storage = self._worker_block_storage + per_vm_task_data
            self.task_data_storage_choice = 'block'


    def populate_freida_details(self):
        """ Populate frieda details in storage plan """
        self.storage_plan['frieda'] = self.frieda

    def populate_application_details(self):
        """ Populate frieda details in storage plan """
        
        self.application['name'] = self._prvsn_request.app_details['name']
        """if self._prvsn_request.source.get('image_id', None) != None:
            self.application['image_id'] = self._prvsn_request.image['image_id']
        self.storage_plan['application'] = self.application"""

        
    def populate_vm_details(self):
        """ Populate VM  details in storage plan.
            Increase the proposed VM count by 1 for master/controller.
        """
        
        self.vm = self._prvsn_request.vm
        self.vm['count'] = self.vm['count'] + self._prvsn_request.master_controller_vm_count
        self.storage_plan['vm'] = self.vm
        
    def populate_master_details(self):
        """ Populate frieda master/controller provision details in storage plan """
        
        self.master['storage_setup'] = {}
        self.master['data_placement'] = {}
        
                
        if self.frieda['mode'] == 'realtime':
            self.master['data_placement']['input']={}
            self.master['data_placement']['input']['storage_choice'] = self.master_input_storage_choice
            if self._prvsn_request.input.get('volume_id', None) != None:
                self.master['data_placement']['input']['volume_id'] = self._prvsn_request.input.get('volume_id', None)
                self.master['data_placement']['input']['volume_input_path'] = self._prvsn_request.input.get('volume_input_path', None)
        self.master['storage_setup']['local_storage_size'] = self.leave_margin(self._master_local_storage)
        self.master['storage_setup']['block_storage_size'] = self.leave_margin(self._master_block_storage)        
        self.master['data_placement']['log']={}
        self.master['data_placement']['log']['storage_choice'] = self.master_log_storage_choice
        
        self.storage_plan['master'] = self.master 
        

    def populate_worker_details(self):        
        """ Populate frieda worker provision details in storage plan """
        
        self.worker['storage_setup'] = {}
        self.worker['data_placement'] = {}        

        if self.frieda['mode'] == 'pre-partitioned':
            self.worker['data_placement']['input']['storage_choice'] = self.worker_input_storage_choice     
                
        if  self._prvsn_request.shared_data != None and self._prvsn_request.shared_data.get('size',0) != 0:
            self.worker['data_placement']['application_shared_data'] ={}
            self.worker['data_placement']['application_shared_data']['storage_choice'] = self.application_shared_data_storage_choice
            #self.worker['application_shared_data']['location'] = self._prvsn_request.shared_data['shared_data_location']
            
        if  self._prvsn_request.source != None and self._prvsn_request.source.get('size',0) != 0:
            self.worker['data_placement']['application_source'] ={}        
            self.worker['data_placement']['application_source']['storage_choice'] = self.application_source_storage_choice
            # Application source details are not needed for irmo.
            #self.worker['application_source']['location'] = self._prvsn_request.source['source_location']
            

        self.worker['data_placement']['task_data']={}            
            
        self.worker['data_placement']['task_data']['storage_choice'] = self.task_data_storage_choice
        self.worker['storage_setup']['block_nfs_storage'] = self.leave_margin(self._worker_block_nfs_storage)
        self.worker['storage_setup']['block_storage_size'] = self.leave_margin(self._worker_block_storage)
        self.worker['storage_setup']['local_storage_size'] = self.leave_margin(self._worker_local_storage)
        self.worker['storage_setup']['local_nfs_storage'] = self.leave_margin(self._worker_local_nfs_storage)
        
        self.worker['data_placement']['log']={}
        self.worker['data_placement']['log']['storage_choice'] = self.worker_log_storage_choice

        self.storage_plan['worker'] = self.worker  
                                    

    def populate_output_details(self):
        """ Populate output in storage plan only 
        if the output storage choice is object """
        
        if self._prvsn_request.output['to_object_or_block'] == "object":
            self.storage_plan['output'] = self.output 
        else:
            self.worker['data_placement']['output']={}
            self.worker['data_placement']['output']['storage_choice'] = 'block'
                                                     

    def populate_storage_plan(self):
        """  populate storage plan details
                
        """ 
        self.populate_master_details()
        self.populate_worker_details()
                      
        self.populate_freida_details()
        self.populate_application_details()
        self.populate_vm_details()
        self.populate_output_details()


    def generate_plan(self):   
    
        """  Generates storage plan for the given application 
             request
                
        """        

        self.vm_details = self.getVMDetailsFromCPS();
        if self.vm_details == None:
            self.error_message = "VM details not available."
            return FAILURE

        # Populate frieda details 
        self.frieda = self._prvsn_request.frieda

        ###################################################
        # Get the storage requirements for master/controller 
        ###################################################


        # Provision storage for application sources 
        self.provision_application_sources()        

        # Provision storage for application shared data
        self.provision_application_shared_data()

        # Provision storage for input data 
        if self.frieda.get('mode',None) != None:           
            self.provision_input() 
        else:
            self.error_message = "FRIEDA MODE is not specified"
            return FAILURE            
                

        # Provision storage for Task Data
        self.provision_task()
        
        # Provision storage for logs on master & worker     
        self.provision_log()
        
        # Provision storage for total output data.
        self.provision_output()
        
        
        # Storage Plan is ready - Populate fields in storage_plan
        self.populate_storage_plan()
    

        return SUCCESS


    def leave_margin(self,value):
        """  Round of the values by adding 100MB 
                
        """
        
        if value > 0:
            value += 100
            value = (value/100) * 100
        return value

    def get_plan(self):
        return self.storage_plan

    def __str__(self):
        return str(self.storage_plan)


if __name__ == "__main__":



    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--spreqfile", dest="spreqfile",
                      help="Application storage plan request", required=True)
    parser.add_argument("-r", "--resource", dest="resourcefile",
                      help="Resource configuration of the infrastructure", required=True)
          
    args = vars(parser.parse_args())
    request = StoragePlanRequest(args['spreqfile'], args['resourcefile'])


    #print request

    # submit request to provision calculator
    plan = StoragePlan(request)
    k=plan.generate_plan()
    if k == FAILURE:
       print  plan.error_message

    print plan

    
