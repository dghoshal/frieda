#!/usr/bin/env python
"""
.. module:: master
   :platform: Unix
   :synopsis:  The FRIEDA master

.. moduleauthor:: Sowmya Balasubramanian <sowmya@es.net>


"""  

import logging
import os
import socket
import pycassa

from time import sleep
from twisted.internet import reactor
from twisted.internet.protocol import Protocol, ServerFactory, ClientFactory
from daemon import Daemon
from frieda.state.statesManager import *
from frieda.state.vectorClock import *
from frieda.state import globals
from keywords import Messages,Data
from frieda.execute.server import FriedaServerFactory, FriedaServerProtocol

from twisted.protocols.basic import LineReceiver

from globusonline.transfer import api_client


FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
LOG = logging.getLogger(__name__)
REQUEST_THRESHOLD=8
BUFFER=4

class IntelliMasterProtocol(FriedaServerProtocol):
    def connectionMade(self):
        FriedaServerProtocol.connectionMade(self)
        self.file_list = self.factory.get_file_list()
        
    def dataReceived(self, requestR):
        LOG.info('dataReceived: start')
        request = requestR.split(globals.separator,1)[0]
        LOG.info('requestR: '+ requestR)
        LOG.info('request: '+ request)
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('Received_request','from_'+str(self.client_ip)+'_req:'+request, globals.VECTOR_CLOCK)
        #sleep(0.1)
        LOG.info('(%s) Request type: %s' % (self.client_ip, request))
        if request.startswith(Messages.MESSAGE_SEMANTICS_RELOAD):

            self.factory.file_list = []
            semantics_info = self.get_content(request)
            self.factory.input_path = semantics_info[1]
            semantics_file = semantics_info[2]
            LOG.info('input directory set to: %s' % self.factory.input_path)
            if semantics_file != 'None':
                self.factory.set_file_list(semantics_file)
            else:
                self.factory.set_file_list()
                
            LOG.info('Semantics file (%s) loaded' % semantics_file)
            self.factory.partition_type = semantics_info[3]
            self.factory.possible_partitions = int(semantics_info[4])
            self.factory.block_size = long(semantics_info[5])
            self.factory.partitioned_data = self.factory.get_partitioned_data()
            
            globals.manager.put('Request','from_'+str(self.client_ip)+': set input_path='+str(semantics_info[1]), globals.VECTOR_CLOCK)
            globals.manager.put('Request','from_'+str(self.client_ip)+': set semantics_file='+str(semantics_info[2]), globals.VECTOR_CLOCK)
            globals.manager.put('Request','from_'+str(self.client_ip)+': set partition_type='+str(semantics_info[3]), globals.VECTOR_CLOCK)
            globals.manager.put('Request','from_'+str(self.client_ip)+': set possible_partitions='+str(semantics_info[4]), globals.VECTOR_CLOCK)
            globals.manager.put('Request','from_'+str(self.client_ip)+': set block_size='+str(semantics_info[5]), globals.VECTOR_CLOCK)

            self.transport.loseConnection()
        elif request == Messages.MASTER_MESSAGE_TRANSFER_METADATA:
            if self.factory.partition_type == Data.DATAPARTITION_REALTIME or self.factory.datamanagement==Data.DATAMANAGEMENT_DATACOORDINATOR:
                num_file_set = len(self.factory.file_list)
                if(num_file_set <= 0):
                    if((self.factory.datamanagement and self.factory.datamanagement == Data.DATAMANAGEMENT_DATACOORDINATOR) and not self.factory.clientFactory.received_all_files):
                        reactor.connectTCP(self.factory.dcn_host, self.factory.dcn_port,self.factory.clientFactory)
                        num_file_set = len(self.factory.file_list)
            else:
                num_file_set = len(self.file_list) 
               
            LOG.info('List of files:' +str(num_file_set)) 
            if num_file_set > 0:
                if self.factory.partition_type == Data.DATAPARTITION_REALTIME or (self.factory.datamanagement and self.factory.datamanagement==Data.DATAMANAGEMENT_DATACOORDINATOR):
                    self.fileset = self.factory.file_list[0]
                    self.factory.file_list.remove(self.fileset)
                else:
                    self.fileset = self.file_list[0]
                    #print self.fileset
                    self.file_list.remove(self.fileset)
                print self.fileset
                self.filenames = self.fileset.split()
                self.num_files = len(self.filenames)
                header = Messages.WORKER_MESSAGE_NUMBER_OF_TRANSFERS
                globals.VECTOR_CLOCK.updateMasterClockByLocal()
                globals.manager.put('FileTransferStart','to_'+str(self.client_ip)+ \
                   ',Req:'+Messages.MASTER_MESSAGE_TRANSFER_METADATA+', filenames='+str(self.filenames), globals.VECTOR_CLOCK)
                mc=globals.VECTOR_CLOCK.nowMasterClock()
                self.transport.write('%s %d%s' % (header, self.num_files, mc) )#+ mc
                globals.VECTOR_CLOCK.updateMasterClockByLocal()
                globals.manager.put('FileTransferEnd','to_'+str(self.client_ip)+ \
                    ':'+Messages.MASTER_MESSAGE_TRANSFER_METADATA+', filenames='+str(self.filenames), globals.VECTOR_CLOCK)

            else:
                #self.factory.clients.remove(self.client_ip)
                #LOG.info('%s disconnected (total live clients: %d)' % (self.client_ip, len(self.factory.clients)))
                self.transport.loseConnection()
                
            #LOG.info('RESERVE BUFFER:' + str(res_buffer))
            if (self.factory.datamanagement != None and self.factory.datamanagement == Data.DATAMANAGEMENT_DATACOORDINATOR):
                #LOG.info('CONNECTING DCN: ' + str(res_buffer)  + ',' + str(num_file_set) + ',' + self.factory.dcn_host)
                #t=type(res_buffer)
                #LOG.info('Res-buffer Type' + str(t))
                #print 'CONNECTING DCN'
                res_buffer = int(self.factory.reserve_buffer)
                if num_file_set <= res_buffer and not self.factory.clientFactory.received_all_files:
                    reactor.connectTCP(self.factory.dcn_host, self.factory.dcn_port,self.factory.clientFactory)
        elif request == Messages.MASTER_MESSAGE_FILE_METADATA:
            self.num_files = len(self.filenames)
            if self.num_files > 0:
                self.filename = self.filenames[0]
                header = Messages.WORKER_MESSAGE_FILE_METADATA
                globals.VECTOR_CLOCK.updateMasterClockByLocal()
                globals.manager.put('FileTransferStart','to_'+str(self.client_ip)+ \
                    ',Req:'+Messages.WORKER_MESSAGE_FILE_METADATA+', filenames='+str(self.filenames), globals.VECTOR_CLOCK)
                mc=globals.VECTOR_CLOCK.nowMasterClock()
                self.transport.write('%s %s%s' % (header, self.filename, mc) )#+ mc
                globals.manager.put('FileTransferEnd','to_'+str(self.client_ip)+ \
                    ',Req:'+Messages.WORKER_MESSAGE_FILE_METADATA+', filenames='+str(self.filenames), globals.VECTOR_CLOCK)

                LOG.info('File to be transferred: %s' % self.filename)
            else:
                notification_msg = Messages.ACKNOWLEDGEMENT
                
                mc=globals.VECTOR_CLOCK.nowMasterClock()
                self.transport.write('%s' % (notification_msg + mc) )#+ mc
        elif request.startswith(Messages.MASTER_MESSAGE_FILE_DATA):
            mc=globals.VECTOR_CLOCK.nowMasterClock()
            remote_file_status = self.get_content(request)
            remote_file_exists = remote_file_status[1]
            if remote_file_exists == 'True':
                self.num_files = len(self.filenames)
                self.filenames.remove(self.filename)
                self.num_files = self.num_files - 1
                if self.num_files == 0:
                    notification_msg = Messages.ACKNOWLEDGEMENT
                    self.transport.write('%s' % (notification_msg + mc) )#+ mc
                else:
                    self.filename = self.filenames[0]
                    header = Messages.WORKER_MESSAGE_FILE_METADATA
                    globals.VECTOR_CLOCK.updateMasterClockByLocal()
                    globals.manager.put('FileTransferStart','to_'+str(self.client_ip)+',Req:'+Messages.MASTER_MESSAGE_FILE_DATA+', filenames='+str(self.filenames), globals.VECTOR_CLOCK)
                    mc=globals.VECTOR_CLOCK.nowMasterClock()
                    self.transport.write('%s %s%s' % (header, self.filename, mc) )#+mc
                    globals.VECTOR_CLOCK.updateMasterClockByLocal()
                    globals.manager.put('FileTransferEnd','to_'+str(self.client_ip)+',Req:'+Messages.MASTER_MESSAGE_FILE_DATA+', filenames='+str(self.filenames), globals.VECTOR_CLOCK)
            else:
                targetdir = remote_file_status[2]
                self.transfer_file(self.filename, targetdir)

                self.filenames.remove(self.filename)
            LOG.info('File (%s) transferred to client: %s' % (self.filename, self.client_ip))
                
        else:
            print request
            print 'Invalid client request'    
                
    def get_content(self, data):
        tokens = data.split(' ')
        return tokens

    def transfer_file(self, filename, targetdir):
        LOG.info('File transfer starting for file %s' % filename)
        #for bytes in self.read_file(os.path.join(self.factory.input_path, filename)):
        #    self.transport.write(bytes)
        input_file = os.path.join(self.factory.input_path, filename)
        #self.target_dir = self.factory.input_path
        scp_cmd='scp -r -i %s -o StrictHostKeyChecking=no %s "%s@%s:%s"' % (self.factory.key_file,
                                                   input_file, 
                                                   self.factory.username,
                                                   self.client_ip, 
                                                   targetdir)
        LOG.info( scp_cmd)
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('FileTransferStart','to_'+str(self.client_ip)+','+Messages.MASTER_MESSAGE_FILE_DATA+', filenames='+str(self.filenames), globals.VECTOR_CLOCK)

        os.system(scp_cmd)
        mc=globals.VECTOR_CLOCK.nowMasterClock()
        self.transport.write(Messages.WORKER_MESSAGE_END_DATA + mc )#+ mc
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('FileTransferEnd','to_'+str(self.client_ip)+
            ',Req:'+Messages.MASTER_MESSAGE_FILE_DATA+', filenames='+str(self.filenames), globals.VECTOR_CLOCK)

        LOG.info('File transfer ended for file %s' % filename)
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('transfer_file_scp_cmd',str(scp_cmd), globals.VECTOR_CLOCK)
        
        
    def read_file(self, file):
        with open(file, 'rb') as file:
            while True:
                data_block = file.read(self.factory.block_size)
                if data_block:
                    yield data_block
                else:
                    break

class MasterCoordinatorProtocol(LineReceiver):
    
    def connectionMade(self):
        LineReceiver.MAX_LENGTH = 1024*1024*64
        LOG.info("Master connected to Data-coordinator")
        
    def lineReceived(self, data):
        LOG.info("Received line data")
        LOG.info(data)
        if (data.startswith(Messages.ACKNOWLEDGEMENT)):
            LOG.info("Sending handshake to coordinator")
            self.transport.write(Messages.DATACOORDINATOR_MESSAGE_HANDSHAKE)
        elif (data.startswith(Messages.MASTER_MESSAGE_TEARDOWN)):
            self.transport.loseConnection()
        elif (data.startswith(Messages.MASTER_MESSAGE_CREDENTIALS)):
            LOG.info("Received handshake from data coordinator")
            datasections = data.split(Messages.MESSAGE_FIELDS_SEPARATOR)
            for datasection in datasections:
                keyvalue = datasection.split(Messages.MESSAGE_HEADER_SEPARATOR)
                if len(keyvalue)>1:
                    if (keyvalue[0].startswith(Messages.CREDENTIALS_FIELD_SOURCE)):
                        self.factory.source = keyvalue[1]
                    if (keyvalue[0].startswith(Messages.CREDENTIALS_FIELD_SOURCE_ALIASES)):
                        self.factory.source_aliases = keyvalue[1]
                        LOG.info(self.factory.source_aliases)
            req_threshold = self.factory.serverfactory.threshold  
            response = '%s:%d' % (Messages.DATACOORDINATOR_MESSAGE_DATAREQUEST, req_threshold)
            LOG.info('Sending: '+response)
            self.transport.write(response)
        else:
            LOG.info("Processing list of files received")
            taskData=data.split(Messages.MESSAGE_VALUES_SEPARATOR)
            response = Messages.DATACOORDINATOR_MESSAGE_TEARDOWN
            open(self.factory.access_semantics_file, 'w').close() # to clear sem file
            semfile = open(self.factory.access_semantics_file,'a')
            for task in taskData:
                splitFiles = task.split()
                LOG.info(splitFiles)
                

                f = []
                filelist = ''
                for file in splitFiles:
                    path,filename=os.path.split(file)
                    full_filename = self.factory.serverfactory.input_path +'/'+filename
                    full_srcname = self.factory.input_path+'/'+filename
                    
                    LOG.info(full_filename)
                    
                    f.append(filename)
                    filelist = ' '.join(f)
                    
                    if(not (self.factory.serverfactory.does_file_exist(full_filename))):
                        LOG.info("File does not exist"+full_filename)
                        self.get_file(self.factory.source, self.factory.source_aliases, full_srcname, self.factory.destination, full_filename)
                
                semfile.write(filelist)
                semfile.write('\n') 
                        
               
            semfile.close() 
            self.factory.serverfactory.update_file_list(self.factory.access_semantics_file)    
            self.transport.write(response)
            self.transport.loseConnection()
            
            if(len(taskData)<=0):
                self.received_all_files = True;

    def get_file(self, source_endpoint, source_aliases, src_file, dst_endpoint, dst_file):
        
        if(self.factory.input_transfer_protocol == "globus"):
            transfer_start = time.time()
            transfer_id = self.factory.transfer_api.submission_id()[2]["value"]
            self.source_ep = source_aliases
            self.dest_ep = dst_endpoint
            self.dest_path = dst_file
            self.source_path = src_file
            print self.source_path
            transfer = api_client.Transfer(transfer_id, self.source_ep, self.dest_ep,
                                       deadline=None, sync_level=None, label=None)
            transfer.add_item(source_path=self.source_path, destination_path=self.dest_path, recursive=False )

            result = self.factory.transfer_api.transfer(transfer)
            task_id = result[2]["task_id"]
            print task_id 
            status = "ACTIVE"
            timeout=30
            while (timeout==None or timeout > 0) and status == "ACTIVE":
                code, reason, data = self.factory.transfer_api.task(task_id, fields="status")
                status = data["status"]
                time.sleep(1)

                if timeout!=None:
                    timeout -= 1

                if status != "ACTIVE":
                    LOG.info( "Task %s complete!" % task_id)
                else:
                    LOG.info( "Task still not complete after %d seconds" % timeout)
        elif(self.factory.input_transfer_protocol == "scp"):
             scp_cmd='scp -o StrictHostKeyChecking=no -i %s %s@%s:%s %s' % (self.factory.key_file, self.factory.username, source_endpoint, src_file, dst_file)
             LOG.info(scp_cmd)
             os.system(scp_cmd)
     
    def lineLengthExceeded(self, line):
        LOG.info("Exceeded line-length size: " + str(len(line)) + ", Max-length: " + str(LineReceiver.MAX_LENGTH))
        
    def connectionLost(self,reason):
        LOG.info("Connection lost")
        print reason
 
class MasterCoordinatorFactory(ClientFactory):   
    protocol = MasterCoordinatorProtocol
    
    def __init__(self, serverfactory, input_transfer_protocol,  username, key_file, destination, input_path, access_semantics_file):
        LOG.info('ClientFactory initialized')
        self.username = username
        self.source = None
        self.source_aliases = None
        self.key_file = key_file
        self.input_path=input_path
        self.input_transfer_protocol = input_transfer_protocol
        self.serverfactory = serverfactory
        self.destination = destination
        self.access_semantics_file = access_semantics_file
        self.received_all_files = False;
        
        if(self.input_transfer_protocol == "globus"):
            self.transfer_api = api_client.TransferAPIClient(self.username,
                                cert_file=self.key_file,
                                key_file=self.key_file,
                                base_url="https://transfer.api.globusonline.org/v0.10")
            
        


class IntelliMaster(object):

    def __init__(self, input_path, semantics_file,  port, partition_type, num_parts, block_size, username, key_file, destination, dcn_user, dcn_host, dcn_port, dcn_dir, input_transfer_protocol, reserve_buffer, threshold, datamanagement):
 
        self.input_path = input_path
        self.port = port
        
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('Master:Server_initializing_start','time', globals.VECTOR_CLOCK) 
            
        self.factory = FriedaServerFactory(self.input_path, semantics_file, partition_type, num_parts, block_size, username, key_file)
        self.factory.protocol = IntelliMasterProtocol        
        
        self.factory.dcn_user = dcn_user
        self.factory.dcn_host = dcn_host
        self.factory.dcn_port = dcn_port
        self.factory.input_transfer_protocol = input_transfer_protocol
        self.factory.reserve_buffer = reserve_buffer
        self.factory.threshold = threshold
        
        self.factory.datamanagement = datamanagement
        
        if((self.factory.datamanagement == None or self.factory.datamanagement != Data.DATAMANAGEMENT_DATACOORDINATOR)):
            self.factory.set_file_list(semantics_file)
            self.factory.partitioned_data = self.factory.get_partitioned_data()
            
        #print self.factory.threshold
        
        #self.factory.clientFactory = MasterCoordinatorFactory(self.factory, input_transfer_protocol, username, key_file, destination, dcn_dir, semantics_file)
        
        if(self.factory.datamanagement != None and self.factory.datamanagement == Data.DATAMANAGEMENT_DATACOORDINATOR):
            LOG.info("IN DATACOORDINATOR MODE")
            self.factory.clientFactory = MasterCoordinatorFactory(self.factory, input_transfer_protocol, dcn_user, key_file, destination, dcn_dir, semantics_file)
            LOG.info(str(self.factory.dcn_user)+ ";" + str(self.factory.dcn_host)+ ";" + str(self.factory.dcn_port))
            reactor.connectTCP(self.factory.dcn_host, self.factory.dcn_port,self.factory.clientFactory)
            
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('Master:Server_initializing_finished','time', globals.VECTOR_CLOCK)
        sleep(0.5)
        LOG.info('Server initialized')

    def listen(self):
        port = reactor.listenTCP(self.port, self.factory)

class MasterDaemon(Daemon):
    def __init__(self, pid, sin, sout, serr, args):
        Daemon.__init__(self, pid, sin, sout, serr)
        self.args = args

    def run(self):
        server = IntelliMaster(self.args.path, self.args.semantics, self.args.port, self.args.partition, self.args.numparts, self.args.blocksize,
                           self.args.username, self.args.key_file, self.args.destination, self.args.dcn_user, self.args.dcn_host, self.args.dcn_port, self.args.dcn_dir,
                           self.args.input_transfer_protocol,self.args.reserve_buffer, self.args.threshold, self.args.datamanagement)
        server.listen()
        reactor.run()

def main(args):
    
    globals.init()
    
    vcSeparator = globals.separator
    globals.VECTOR_CLOCK = VectorClock(vcSeparator)
    role = 'master'
    print 'master.py started.'
    globals.manager = stateManagerClient(role, 'file', os.environ['HOME'], socket.gethostname(), getLanIP(), 9160, vcSeparator)
    print 'master.py: manager created.'
    
    globals.VECTOR_CLOCK.updateMasterClockByLocal()
    globals.manager.put('Master_start','time', globals.VECTOR_CLOCK)
    print 'master.py: a k-v pair put.'
    
    globals.manager.runCommand('cpuinfo','cat /proc/cpuinfo', globals.VECTOR_CLOCK)
    globals.manager.runCommand('meminfo','cat /proc/meminfo', globals.VECTOR_CLOCK)
    globals.manager.runCommand('osinfo','uname -a', globals.VECTOR_CLOCK)
    globals.manager.runCommand('env','env', globals.VECTOR_CLOCK)
    sleep(0.5) 
###########################################

    pid = '/tmp/master.pid'
    stdin = '/dev/null'
    stdout = '/tmp/master_daemon.out'
    stderr = '/tmp/master_daemon.err'


    mdaemon = MasterDaemon(pid, stdin, stdout, stderr, args)
    
    if 'start' == args.action:
        mdaemon.start()
    elif 'stop' == args.action:
        mdaemon.stop()
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('Master_end','time', globals.VECTOR_CLOCK)


if __name__ == '__main__':
    main()
