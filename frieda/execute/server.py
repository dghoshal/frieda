#!/usr/bin/env python

"""
.. module:: server
   :platform: Unix
   :synopsis:  The FRIEDA server. This defines a basic server. 
               Known subclasses: master, datacoordinator

.. moduleauthor:: Sowmya Balasubramanian <sowmya@es.net>


"""  

import logging
import os
import socket
import pycassa

from time import sleep
from twisted.internet import reactor
from twisted.internet.protocol import Protocol, ServerFactory
from daemon import Daemon
from frieda.state.statesManager import *
from frieda.state.vectorClock import *
from frieda.execute.keywords import *

from frieda.state import globals
from keywords import Messages,Data

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
LOG = logging.getLogger(__name__)

#Basic server protocol. Defines only connectionMade and connectionLost methods. 
#Known subclasses: IntelliMasterProtocol, DataCoordinatorProtocol
class FriedaServerProtocol(Protocol):
    
    # When a connection is established, the IP address is registered and a ACK message is sent
    def connectionMade(self):
        self.client_ip = self.transport.getPeer().host
        self.factory.clients.append(self.client_ip)
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('connectionMade','to_'+str(self.client_ip), globals.VECTOR_CLOCK)
        LOG.info('%s connected (total live clients: %d)' % (self.client_ip, len(self.factory.clients)))
        ack = Messages.ACKNOWLEDGEMENT
        mc=globals.VECTOR_CLOCK.nowMasterClock()
        self.transport.write(ack + mc)#+mc
    
    # When a connection is lost, the client IP list is updated.
    def connectionLost(self, reason):
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('connectionLost','to_'+str(self.client_ip)+'_reason:'+str(reason), globals.VECTOR_CLOCK)
        self.factory.clients.remove(self.client_ip)
        LOG.info('%s disconnected (total live clients: %d)' % (self.client_ip, len(self.factory.clients)))
        LOG.info('Connection lost: %s' % reason)
        

#Basic server factory.   
#Known subclasses: DataCoordinatorServerFactory
#NOTE: IntelliMaster uses the FriedaServerFactory
class FriedaServerFactory(ServerFactory):
    
    protocol = FriedaServerProtocol

    def __init__(self, input_path, access_semantics_file, partition_type, num_partitions, block_size, ssh_user, ssh_key):
    
        LOG.info('ServerFactory initialized')
        self.input_path = input_path
        self.file_list = []
        self.clients = []
        self.partition_type = partition_type
        self.possible_partitions = num_partitions
        self.partitioned_data = []
        self.block_size = block_size
        self.username=ssh_user
        self.key_file=ssh_key
        
        
        globals.VECTOR_CLOCK.updateMasterClockByLocal()
        globals.manager.put('input_path',input_path, globals.VECTOR_CLOCK)
        globals.manager.put('access_semantics_file',access_semantics_file, globals.VECTOR_CLOCK)
        globals.manager.put('partition_type',partition_type, globals.VECTOR_CLOCK)
        globals.manager.put('num_partitions',num_partitions, globals.VECTOR_CLOCK)
        globals.manager.put('block_size',block_size, globals.VECTOR_CLOCK)
        globals.manager.put('ssh_user',ssh_user, globals.VECTOR_CLOCK)
        globals.manager.put('ssh_key',ssh_key, globals.VECTOR_CLOCK)
    
    def set_file_list(self, access_semantics_file=None):
        self.file_list = []
        self.access_semantics_file = access_semantics_file
        if self.access_semantics_file == None:
            os.chdir(self.input_path)
            for files in os.listdir("."):
                if not os.path.isdir(files):
                    self.file_list.append(files)
        else:
            as_file = open(self.access_semantics_file, 'r')
            self.file_list = as_file.read().splitlines()

    def update_file_list(self, access_semantics_file):
        
        self.access_semantics_file = access_semantics_file
        if self.access_semantics_file :
            as_file = open(self.access_semantics_file, 'r')
            tmp_list = as_file.read().splitlines()
            self.file_list.extend(tmp_list)
            self.partitioned_data = self.get_partitioned_data()
            
    def remove_element(self, key):
        self.file_list.remove(key)
        
    def get_partitioned_data(self):
        global_partition = []
        if self.partition_type == Data.DATAPARTITION_EQUAL:
            num_partitions = self.possible_partitions 
            num_filesets = len(self.file_list)
            files_per_partition = num_filesets/num_partitions
            last_partition_size = files_per_partition
            if num_filesets % num_partitions != 0:
                last_partition_size += (num_filesets % num_partitions)
            index = 1
            allocated_partitions = 1
            local_partition = []
            for files in self.file_list:
                local_partition.append(files)
                if index < files_per_partition:
                    index = index + 1
                else:
                    if allocated_partitions == num_partitions - 1:
                        files_per_partition = last_partition_size
                    global_partition.append(local_partition)
                    local_partition = []
                    allocated_partitions = allocated_partitions + 1
                    index = 1
        else:
            global_partition = self.file_list           
        LOG.info('Partition-set: %s' % global_partition)
        return global_partition
    
    def get_file_list(self):
        file_list = []
        if self.partition_type == Data.DATAPARTITION_EQUAL:
            if len(self.partitioned_data) != 0:
                file_list = self.partitioned_data[0]
                self.partitioned_data.remove(file_list)
        else:
            file_list = self.partitioned_data[:]
        return file_list
    
    def does_file_exist(self, file):
        if os.path.isfile(file):
            try:
                with open(file) as f:pass
                return True
            except IOError as e:
                return False
        else:
            return False
