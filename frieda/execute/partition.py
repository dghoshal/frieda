"""
.. module:: partition
   :platform: Unix, Mac
   :synopsis:  The partition generator

.. moduleauthor:: Devarshi Ghoshal <dghoshal@lbl.gov>


"""

import fnmatch
import os
import random
from collections import namedtuple
import common


VisitorArgs = namedtuple('VisitorArgs', 'pattern path files size_limit sizes')


def cross_partition(input_dir, pattern, semantics_file, mode=0775, size_limit=None):
    """
    Create a cross partition semantic file


    :param input_dir: the base directory for the input
    :param pattern: file pattern to look for
    :param semantics_file: semantics file name
    :param mode: the file mode
    :type mode: int
    :param size_limit: The limit on the total size in GB. If the value is None there is not limit
    :type size_limit: int
    :returns: total size in GB, total records, file count
    :rtype: tuple
    """
    f = common.openWithMode(semantics_file, mode)
    file_list, total_size = getFiles(input_dir, pattern, size_limit)
    total_records = 0
    startIndex = 0
    file_pairs = []
    for first in file_list:
        startIndex += 1
        for second in file_list[startIndex:]:
            file_pairs.append((first, second))
            f.write(first + ' ' + second + '\n')
            total_records += 1
    f.close()
    return total_size, total_records, len(file_list)


def exclusive_partition(input_dir, pattern, semantics_file, mode=0775, size_limit=None):
    """

    :param input_dir: the base directory for the input
    :param pattern: file pattern to look for
    :param semantics_file: semantics file name
    :param mode: the file mode
    :type mode: int
    :returns: total size in GB, total records, file count
    :rtype: tuple
    """
    f = common.openWithMode(semantics_file, mode)
    file_list, total_size = getFiles(input_dir, pattern, size_limit)
    file_pairs = []
    total_records = 0
    for first, second in zip(*[iter(file_list)] * 2):
        file_pairs.append((first, second))
        f.write(first + ' ' + second + '\n')
        total_records += 1
    if len(file_list) % 2 != 0:
        last = file_list[-1:]
        for l in last:
            f.write(l + ' ' + l + '\n')
        total_records += 1
    f.close()
    return total_size, total_records, len(file_list)


def one2all_partition(input_dir, pattern, semantics_file, mode=0775, size_limit=None):
    """

    :param input_dir: the base directory for the input
    :param pattern: file pattern to look for
    :param semantics_file: semantics file name
    :param mode: the file mode
    :type mode: int
    :returns: total size in GB, total records, file count
    :rtype: tuple
    """
    f = common.openWithMode(semantics_file, mode)
    file_list, total_size = getFiles(input_dir, pattern, size_limit)
    file_pairs = []
    num_files = len(file_list)
    rand = random.randint(0, num_files - 1)
    random_file_to_compare = file_list[rand]
    total_records = 0
    for img in file_list:
        file_pairs.append((random_file_to_compare, img))
        f.write(random_file_to_compare + ' ' + img + '\n')
        total_records += 1
    f.close()
    return total_size, total_records, len(file_list)

#This method compares first file to the rest of the files
def commonfile_partition(input_dir, pattern, semantics_file, mode=0775, size_limit=None):
    """

    :param input_dir: the base directory for the input
    :param pattern: file pattern to look for
    :param semantics_file: semantics file name
    :param mode: the file mode
    :type mode: int
    :returns: total size in GB, total records, file count
    :rtype: tuple
    """
    f = common.openWithMode(semantics_file, mode)
    file_list, total_size = getFiles(input_dir, pattern, size_limit)
    file_pairs = []
    num_files = len(file_list)
    file_to_compare = file_list[0]
    total_records = 0
    i=1
    while i<num_files:
        img=file_list[i]
        file_pairs.append((file_to_compare, img))
        f.write(file_to_compare + ' ' + img + '\n')
        total_records += 1
        i += 1
    f.close()
    return total_size, total_records, len(file_list)

def no_partition(input_dir, pattern, semantics_file, mode=0775, size_limit=None):
    """

    :param input_dir: the base directory for the input
    :param pattern: file pattern to look for
    :param semantics_file: semantics file name
    :param mode: the file mode
    :type mode: int
    :returns: total size in GB, total records, file count
    :rtype: tuple
    """
    f = common.openWithMode(semantics_file, mode)
    files, total_size = getFiles(input_dir, pattern, size_limit)
    for file_ in files:
        f.write(file_ + '\n')
    f.close()
    num_files = len(files)
    return total_size, num_files, num_files


def getFiles(path, pattern, size_limit=None):
    """
    Retrieve all the files with the given pattern


    :param path: the directory to search
    :param pattern: the file pattern to search for
    :return: the files matching the pattern
    :param size_limit: The limit on the total size in GB. If the value is None there is not limit
    :type size_limit: int
    :rtype: list
    """

    if size_limit:
        # convert to bytes
        size_limit = size_limit * 1024 * 1024 * 1024
    visitor_args = VisitorArgs(pattern=pattern, path=path, files=[], size_limit=size_limit,
                               sizes=[])
    os.path.walk(path, _visitDirectory, visitor_args)
    return visitor_args.files, float(sum(visitor_args.sizes)) / (1024 * 1024 * 1024)


def get_directory_size(p):
   from functools import partial
   prepend = partial(os.path.join, p)
   return sum([(os.path.getsize(f) if os.path.isfile(f) else getFolderSize(f)) for f in map(prepend, os.listdir(p))])

def _visitDirectory(visitor_args, directory_name, names):
    sum_sizes = sum(visitor_args.sizes)
    if not visitor_args.size_limit or sum_sizes < visitor_args.size_limit:
        files = []
	for f in fnmatch.filter(names, visitor_args.pattern):
	    add_file = True
	    directory = "%s/%s" % (directory_name, f)
	    if os.path.isfile(directory):
		size_in_bytes = os.path.getsize(directory)
	    else:
		size_in_bytes = get_directory_size(directory) 
	    if not visitor_args.size_limit or sum_sizes < visitor_args.size_limit:
                if visitor_args.size_limit:
                    add_file = (sum_sizes + size_in_bytes) <= visitor_args.size_limit

            if add_file:
                sum_sizes += size_in_bytes
                visitor_args.sizes.append(size_in_bytes)
                files.append(('%s/%s' % (directory_name, f)).replace(visitor_args.path + '/', ''))

        visitor_args.files.extend(files)


def main(args):
    if args.pair_type == 'cross':
        total_size, total_records, file_count = cross_partition(args.input_dir, "*.%s" % args.file_suffix,
                                                                args.semantics_file,
                                                                size_limit=args.max_input_size)
    elif args.pair_type == 'exclusive':
        total_size, total_records, file_count = exclusive_partition(args.input_dir, "*.%s" % args.file_suffix,
                                                                    args.semantics_file,
                                                                    size_limit=args.max_input_size)
    elif args.pair_type == 'one2all':
        total_size, total_records, file_count = one2all_partition(args.input_dir, "*.%s" % args.file_suffix,
                                                                  args.semantics_file,
                                                                  size_limit=args.max_input_size)
    elif args.pair_type == 'commonfile':
        total_size, total_records, file_count = commonfile_partition(args.input_dir, "*.%s" % args.file_suffix,
                                                                  args.semantics_file,
                                                                  size_limit=args.max_input_size)
    else:
        total_size, total_records, file_count = no_partition(args.input_dir, "*.%s" % args.file_suffix,
                                                             args.semantics_file,
                                                             size_limit=args.max_input_size)
   
    print "Semantic file %s created -  data_size_gb:%f record_count:%d file_count:%d" % (
        args.semantics_file, total_size, total_records, file_count)


if __name__=='main':
    main()
