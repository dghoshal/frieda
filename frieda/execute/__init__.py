
#!/usr/bin/python
"""
.. module:: frieda
   :platform: Unix, Mac
   :synopsis:  This module contains the entry point for starting frieda


.. moduleauthor:: Val Hendrix <vchendrix@lbl.gov>


"""
import os
import sys
import shutil
import ConfigParser

import argparse

import controller
import master
import partition
import worker
import datacoordinator


def _config():
    """ 
    Load the FRIEDA configuration files from ~/.frieda/config. If it doesn't
    exist create it and exit.
    """
    templatesDir = "%s" % os.path.dirname(os.path.abspath(sys.modules[__name__].__file__))
    configDir = os.path.expanduser("~/.frieda")
    configFileName = os.path.expanduser("%s/config" % configDir)
    if not os.path.exists(configFileName):
        if not os.path.exists(configDir):
            os.makedirs(configDir, 0700)
        shutil.copyfile("%s/config.tpl" % templatesDir, configFileName)
        print "A sample configuration file was created in %s ." % configFileName
        print "Please edit"

    config = ConfigParser.RawConfigParser()
    config.read(configFileName)
    return config, configFileName


def _csv(value):
    """ csv type to be used as a command line option.
        Splits the given string using the comma as a delimiter.
    
        :param value: a comma delimited string
        :type value: str.
        :returns:  a list
        
    """
    if value and len(value) >= 0:
        return value.split(",")
    else:
        return []


def _parse_known_args(parser):
    """
    Use the given parser to parse the known args. Ignores the help command.
    
    :param parser: the configuration parser
    :type parser: ConfigParser.RawConfigParser
    """
    # Suppress the help.  We want to get the configuration file.
    args = filter(lambda a: a != '-h' and a != '--help', sys.argv)
    # Add something to prevent an error
    if len(args) <= 1:
        args.append("control")
    elif len(args) >= 2:
        args[1] = "control"
    args, rargs = parser.parse_known_args(args[1:])
    return args


def _addMasterParser(subparsers, master_config):
    parser_master = subparsers.add_parser('master',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Launch a FRIEDA master. The
    master and the workers operate in the execution plane and
    manage the execution of the program.
    
    The master distributes and transfers the files to the respective 
    workers based on the selected data management strategy.
    """)

    parser_master.add_argument("-a", "--action",
                               dest="action",
                               choices=['start', 'stop'],
                               required=True,
                               help='Controls Master as daemon')

    parser_master.add_argument("-i", "--inputdir",
                               dest="path",
                               #required=True,
                               default="./",
                               help='input directory path')
    parser_master.add_argument('-p', '--port',
                               type=int,
                               dest='port',
                               default=1234,
                               help='server listening port')
    parser_master.add_argument('-s', '--semantics',
                               dest='semantics',
                               help='access semantics file')
    parser_master.add_argument('-P', '--partition',
                               dest='partition',
                               choices=['realtime', 'equal', 'all',],
                               default='realtime',
                               help='type of data-partitioning (realtime/equal/all)')
    parser_master.add_argument('-n', '--numparts',
                               type=int,
                               dest='numparts',
                               default=1,
                               help='number of partitions (only valid in case of equal partition-type)')
    parser_master.add_argument('-b', '--blocksize',
                               type=long,
                               dest='blocksize',
                               default=40960,
                               help='file-transfer block-size')
    parser_master.add_argument('-e', '--destination',
                               dest='destination',
                               default='localhost',
                               help='destination host for file transfers')
    parser_master.add_argument('--datamanagement',
                               dest='datamanagement',
                               choices=['datacoordinator'],
                               help='mode for data management')
    parser_master.add_argument('--dcn_host',
                               dest='dcn_host',
                               help='data coordinator host')
    parser_master.add_argument('--dcn_port',
                               type=int,
                               dest='dcn_port',
                               help='ddn source port')
    parser_master.add_argument('--dcn_user',
                               dest='dcn_user',
                               help='ddn user name')
    parser_master.add_argument('--dcn_dir',
                               dest='dcn_dir',
                               help='ddn source directory')
    parser_master.add_argument('-f','--transferprotocol',
                               choices=['scp','globus'],
                               dest='input_transfer_protocol',
                               help='ftp protocol to be used')
    parser_master.add_argument('-u','--username',
                               dest='username',
                               help='worker username')
    parser_master.add_argument('-k','--key_file',
                               dest='key_file',
                               help='worker key file location')
    parser_master.add_argument('-r','--reserve_buffer',
                               dest='reserve_buffer',
                               help='reserve buffer of files to be maintained by master. determines when to request for new files')
    parser_master.add_argument('-t','--threshold',
                               dest='threshold',
                               type=int,
                               help='threshold for number of files to be requested by datacoordinator')


    master_config['func'] = master.main
    parser_master.set_defaults(**master_config)


def _addControllerParser(parser, subparsers, control_config):
    parser_controller = subparsers.add_parser('control',
                                              formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                              help=""" Launch a FRIEDA control process.
                                            The controller (and the parttion generation algorithm) are
                                            in the control plane and are responsible for setting up the
                                            environment for data management and program execution. 
                                            
                                            The controller communicates the data management information to the master.
                                            """)

    control_config['func'] = controller.main
    parser_controller.set_defaults(**control_config)
    parser_controller.add_argument('-C', '--controlType',
                                   dest='control',
                                   choices=['master', 'worker'],
                                   default='master',
                                   help='Specify to control either the master or the worker.')
    parser_controller.add_argument('-s', '--server',
                                   dest='server',
                                   default='localhost', help='IP of the master running')
    parser_controller.add_argument('-p', '--port',
                                   dest='port',
                                   type=int,
                                   default=1234,
                                   help='master listening port')
    parser_controller.add_argument('-P', '--partition',
                                   dest='partition',
                                   choices=['realtime', 'equal', 'all'],
                                   default='realtime',
                                   help='type of data-partitioning (realtime/equal/all)')
    parser_controller.add_argument('-m', '--multicore',
                                   action='store_true',
                                   dest='multicore',
                                   default=False,
                                   help='flag to indicate if multicore has to be used')

    args = _parse_known_args(parser)

    if args.__dict__.has_key('control') and args.control == 'master':
        parser_controller.add_argument('-b', '--blocksize',
                                       type=long,
                                       dest='blocksize',
                                       help='file-transfer block-size (in bytes)')
        parser_controller.add_argument('-i', '--inputdir',
                                       dest='inputdir',
                                       required=True,
                                       help='Input directory on the master server from where the files are to be transferred')
        parser_controller.add_argument('-f', '--semanticsfile',
                                       dest='semantics',
                                       required=True,
                                       help="""Semantics file to identify the file access pattern of an application.
                                       (Local to the controller)""")
        parser_controller.add_argument('-n', '--numparts',
                                       type=int,
                                       dest='numparts',
                                       default=1,
                                       help='Number of partitions (only valid in case of equal partition-type)')
        parser_controller.add_argument('-d','--dcn_host',
                               dest='dcn_host',
                               help='data coordinator host')
        parser_controller.add_argument('-q','--dcn_port',
                               type=int,
                               dest='dcn_port',
                               help='ddn source port')
        parser_controller.add_argument('-t','--transferprotocol',
                               choices=['scp'],
                               dest='input_ftp_protocol',
                               help='ddn source port')
        parser_controller.add_argument('--datamanagement',
                               dest='datamanagement',
                               choices=['datacoordinator'],
                               help='mode for data management')
    elif args.__dict__.has_key('control') and args.control == 'worker':
        parser_controller.add_argument('-H', '--hosts',
                                       type=_csv,
                                       dest='hosts',
                                       required=True,
                                       help='List of hostnames')
        parser_controller.add_argument('-x', '--executable',
                                       dest='executable',
                                       required=True,
                                       help='command to run with parameters')
        parser_controller.add_argument('-t', '--targetdir',
                                       dest='targetdir',
                                       required=True,
                                       help='target directory where the output files will be written')

        parser_controller.add_argument('-f', '--ftp',
                                   dest='ftp',
                                   choices=['globusonline','scp', 'None'],
                                   default='None',
                                   help='ftp protocol to be used to move output data')                               
                               
        parser_controller.add_argument('-l', '--file-list',
                                   dest='file_list',                               
                                   help='List of output files to be transferred to destination machine. Wild characters also supported')   
                               
        parser_controller.add_argument('-D', '--destination_machine',
                                   dest='machine',                               
                                   help='Target machine/endpoint to which data need to be transferred.')   
                               
        parser_controller.add_argument('-o', '--output-dir',
                                   dest='output_dir',                               
                                   help='Ouput directory into which data need to be transferred.')   

        parser_controller.add_argument('-u', '--uid',
                                   dest='uid',                               
                                   help='Target machine/endpoint user id')   
                               
        parser_controller.add_argument('-k', '--key',
                                   dest='key',                               
                                   help='if FTP is scp, it is destination machines private key else it is globusonline key ')                                              


def _addPartitionParser(subparsers):
    parser_partitioner = subparsers.add_parser('partition',
                                               formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                               help=""" Create a FRIEDA semantics file from input data.
                                        """)
    parser_partitioner.set_defaults(func=partition.main)
    parser_partitioner.add_argument('input_dir', help='The directory where the application data is.')
    parser_partitioner.add_argument('file_suffix',
                                    help='The suffix of the files in the input directory to create the semantics for.')
    parser_partitioner.add_argument('pair_type',
                                    choices=['cross', 'exclusive', 'one2all', 'commonfile','none'],
                                    help="""How to pair the files or not. 
                                            cross - compares each file to every other file.
                                            exclusive - pairs are exclusive.
                                            one2all - randomly choose one file and compares with other files including itself'
                                            commonfile- first file to every other file excluding itself
                                            none -  list of files. no pairing
                                             """)
    parser_partitioner.add_argument('semantics_file',
                                    help='Name of the semantics file to create.')
    parser_partitioner.add_argument('-m', dest='max_input_size',
                                    type=int,
                                    default=None,
                                    help='If this exists, it is the total size in GB of all the files to be used.')


def _addWorkerParser(subparsers):
    parser_worker = subparsers.add_parser('worker',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Launch a FRIEDA worker process
                                        """)

    parser_worker.set_defaults(func=worker.main)
    parser_worker.add_argument('-s', '--server',
                               dest='server',
                               default='localhost',
                               required=True,
                               help='IP of the server running')
    parser_worker.add_argument('-p', '--port',
                               type=int,
                               dest='port',
                               default=1234,
                               help='server listening port')
    parser_worker.add_argument('-t', '--targetdir',
                               dest='targetdir',
                               default='.',
                               help='target directory where the output files will be written')
    parser_worker.add_argument('-x', '--executable',
                               dest='executable',
                               default=None,
                               help="""name of the executable with its path.
                               Executable command must be given in order to partition data in real-time""")
    parser_worker.add_argument('-d', '--do-housekeep',
                               action='store_true',
                               dest='housekeep',
                               default=False,
                               help='delete transferred files after processing')
    parser_worker.add_argument('-P', '--partition',
                               dest='partition',
                               choices=['realtime', 'equal', 'all', 'datacoordinator'],
                               default='realtime',
                               help='type of data-partitioning (realtime/equal/all/datacoordinator)')
    parser_worker.add_argument('-f', '--ftp',
                               dest='ftp',
                               choices=['globusonline','scp', 'None'],
                               default='scp',
                               help='ftp protocol to be used to move output data')                               
                           
    parser_worker.add_argument('-l', '--file-list',
                               dest='file_list',                               
                               help='List of output files to be transferred to destination machine. Wild characters also supported')   
                           
    parser_worker.add_argument('-D', '--destination_machine',
                               dest='machine',                               
                               help='Target machine/endpoint to which data need to be transferred.')   
                           
    parser_worker.add_argument('-o', '--output-dir',
                               dest='output_dir',                               
                               help='Ouput directory into which data need to be transferred.')   

    parser_worker.add_argument('-u', '--uid',
                               dest='uid',                               
                               help='Target machine/endpoint user id')   
                           
    parser_worker.add_argument('-k', '--key',
                               dest='key',                               
                               help='if FTP is scp, it is destination machines private key else it is globusonline key ')                                   
                               
                      
def _addDataCoordinatorParser(subparsers):
    parser_coordinator = subparsers.add_parser('datacoordinator',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Launch a FRIEDA datacoordinator
                                        """)

    parser_coordinator.set_defaults(func=datacoordinator.main)
    parser_coordinator.add_argument('--src_host',
                               dest='src_host',
                               default='localhost',
                               help='IP address or hostname of the data source')
    parser_coordinator.add_argument('--aliases',
                               dest='src_aliases',
                               default=None,
                               help='aliases for data source')
    parser_coordinator.add_argument('-a', '--action',
                                    required=True,
                                    dest='action',
                                    help='action=start|stop the datacoordinator')
    
    parser_coordinator.add_argument("-i", "--inputdir",
                               dest="path",
                               #required=True,
                               help='input directory path')
    parser_coordinator.add_argument('-p', '--port',
                               type=int,
                               dest='port',
                               default=1234,
                               help='server listening port')
    parser_coordinator.add_argument('-s', '--semantics',
                               dest='semantics',
                               help='access semantics file')
    parser_coordinator.add_argument('-P', '--partition',
                               dest='partition',
                               choices=['realtime', 'equal', 'all'],
                               default='realtime',
                               help='type of data-partitioning (realtime/equal/all)')
    parser_coordinator.add_argument('-n', '--numparts',
                               type=int,
                               dest='numparts',
                               default=1,
                               help='number of partitions (only valid in case of equal partition-type)')
    parser_coordinator.add_argument('-b', '--blocksize',
                               type=long,
                               dest='blocksize',
                               default=40960,
                               help='file-transfer block-size')

    
    

def main(parser):
    config, configFilename = _config()
    if not config:
        print "Configuration file does not exist: %s " % configFilename
        sys.exit()

    try:

        main_config = dict(config.items("main"))
        master_config = dict(config.items("master") + main_config.items())
        control_config = dict(config.items("control") + main_config.items())

    except ConfigParser.NoSectionError, e:
        raise Exception("There is a problem with your configuration %s: %s" % (configFilename, str(e)))


    #Top Level Parser
    #parser = argparse.ArgumentParser(description="",
    #                                 prog="frieda",
    #
    #                                  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-l', '--logdir',
                        dest='log_dir',
                        default='./logs',
                        help='The directory for all of the logs')

    parser.set_defaults(**main_config)

    subparsers = parser.add_subparsers()
    _addControllerParser(parser, subparsers, control_config)
    _addMasterParser(subparsers, master_config)
    _addWorkerParser(subparsers)
    _addPartitionParser(subparsers)
    _addDataCoordinatorParser(subparsers)

    #args = parser.parse_args()
    #args.func(args)


if __name__ == '__main__':
    main()
