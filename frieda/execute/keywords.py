"""
.. module:: keywords
   :platform: Unix
   :synopsis:  All the common constants used by frieda.execute are defined here

.. moduleauthor:: Sowmya Balasubramanian <sowmya@es.net>


"""  

#All the constants(like message headers, message fields, etc) used for exchanging messages
class Messages():
    
    
    CREDENTIALS_FIELD_SOURCE = 'source'
    CREDENTIALS_FIELD_SOURCE_ALIASES = 'src-aliases'
    DATACOORDINATOR_MESSAGE_TEARDOWN =  '__DONE__'
    DATACOORDINATOR_MESSAGE_HANDSHAKE = '__DATA_CREDENTIALS_REQUEST__'
    DATACOORDINATOR_MESSAGE_DATAREQUEST = '__DATA_REQUEST__'
   
    ACKNOWLEDGEMENT = '__ACK__'
    
    MASTER_MESSAGE_CREDENTIALS = '__DATA_TRANSFER_CREDENTIALS__'
    MASTER_MESSAGE_TEARDOWN = '__TEAR_DOWN__'
    
    
    MESSAGE_SEMANTICS_RELOAD = '__RELOAD_SEMANTICS_FILE__'
    MASTER_MESSAGE_TRANSFER_METADATA = '__SEND_TRANSFER_METADATA__'
    MASTER_MESSAGE_FILE_METADATA = '__SEND_FILE_METADATA__'
    MASTER_MESSAGE_FILE_DATA = '__SEND_FILE_DATA__'
    
    WORKER_MESSAGE_NUMBER_OF_TRANSFERS = '__NUM_TRANSFERS__'
    WORKER_MESSAGE_FILE_METADATA = '__FILE_METADATA__'
    #WORKER_MESSAGE_ACKNOWLEDGEMENT = ACKNOWLEDGEMENT
    WORKER_MESSAGE_END_DATA = '__END_DATA__'
    
    MESSAGE_FIELDS_SEPARATOR = ';'
    MESSAGE_HEADER_SEPARATOR = ':'
    MESSAGE_VALUES_SEPARATOR = ','

#Constants related to datamanagement    
class Data():
    DATAPARTITION_REALTIME = 'realtime'
    DATAPARTITION_EQUAL = 'equal'
    DATAMANAGEMENT_DATACOORDINATOR = 'datacoordinator'

#Constants related to server   
class Server():
    SERVER_TYPE_DATACOORDINATOR = 'datacoordinator'
    SERVER_TYPE_MASTER = 'master'

    
    
    


