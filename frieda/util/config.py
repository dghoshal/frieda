import os

import yaml

import cloudutil


class CloudConfig:
    def __init__(self, yamlFile):
        self.yamlFile = yamlFile
        if yamlFile:
            yamlStr = cloudutil.getTemplateFileContent(yamlFile, os.environ)

        self.yamlDict = yaml.load(yamlStr)

    def cloud(self):

        if self.yamlDict.has_key('cloud'):
            return self.yamlDict['cloud']
        else:
            return None

    def orchestration(self, name):

        if self.yamlDict['orchestration'].has_key(name):
            return self.yamlDict['orchestration'][name]
        return None

    def roles(self, name):
        if self.yamlDict['role'].has_key(name):
            return self.yamlDict['role']
        return None

    def role(self, name):
        if self.yamlDict['role'].has_key(name):
            return self.yamlDict['role'][name]
        return None

    def provision(self, name):
        if self.yamlDict.has_key('provision'):
            if self.yamlDict['provision'].has_key(name):
                return self.yamlDict['provision'][name]
        return None

    def actions(self, name):
        if self.yamlDict.get('actions', None) != None:
            return self.yamlDict['actions'].get(name, None)
        return None
 
    def frieda(self, name):
    	if self.yamlDict['frieda'].has_key(name):
            return self.yamlDict['frieda'][name]
        return None
    
    def __repr__(self):
        return str(self.yamlDict)

    def __str__(self):
        return yaml.dump(self.yamlDict)
        

