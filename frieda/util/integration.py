#!/usr/bin/env python

import argparse
import commands
import sys
import time
import os
import pdb
import shutil
import logging
import frieda
import yaml
from frieda.util import monkey
import copy
import re

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, filename="test_runner.log")
LOG = logging.getLogger(__name__)

def friedamonkey(config_file, phase, action, nodes=None):

    """
    Run preparation for the given nodes

    :param config_file:  The configuration file to use
    :param action: The contextualization action
    :param nodes: The nodes to contextualize
    :type nodes: list
    :return:
    """
    
    LOG.info("\n*****************\nRun '%s: %s' Start\n****************\n" % (phase, action))
    start = time.time()
    command_args = {'config': config_file, 'action': action, 'nodes': nodes}

    LOG.info("Executing monkey with arguments : %s" % command_args)
    main_args = [config_file,phase, action]
    if nodes:
        main_args.extend(nodes.split(" "))
    monkey.main(main_args)
    end = time.time()
    LOG.info("Run '%s' Time: %f" % (action, (end - start)))
    LOG.info("\n*****************\nEnd '%s: %s' End\n****************\n" % (phase, action))

def populate_default_values(default_yaml):
    start="%("
    end=")s"
    vars = {}
    fh=open(default_yaml)
    for line in fh:
        m=re.findall(re.escape("%(")+".*?"+re.escape(")s"),line)
        for i in m:
            vars[i.replace("%(","").replace(")s","")]=None
    fh.close()
    return vars



def addDefaultActions(cmDict, config_file):
    """
        Modify the configuration file to include default actions like 
        os-setup, prepare master and worker data directories, copy the
        application source and place data on master
    :param cmDict:
    :param ssh_key:
    """

    default_yaml = os.path.join( os.environ['FRIEDA_HOME'] , "resources/configs/default.yaml" )

    """ populate default_params of the default_yaml with None values. """
    default_params = populate_default_values(default_yaml)

    fh = open(default_yaml)    
    format_dict=default_params
    format_dict.update(os.environ.__dict__['data'])
    format_dict.update(cmDict['application'])
    format_dict.update(cmDict['application']['input_data'])

    
    if cmDict['application'].get('output_data'):
        ftp = cmDict['application']['output_data'].get('ftp',None)
        if ftp:
            format_dict['file_list'] = ",".join(cmDict['application']['output_data']['files'])
            format_dict.update(cmDict['application']['output_data'])        
        
        
    sto = cmDict['application']['input_data'].get('storage',None)
    if sto:
        format_dict.update(sto)
    format_dict.update(cmDict['frieda'])
    format_dict.update(cmDict['cloud'])
    format_dict.update(cmDict['cloud'])    
    format_dict.update({'config': os.path.dirname(config_file)})

    defaultDictString = fh.read() % format_dict
    fh.close()
    defaultDict = yaml.load(defaultDictString)

    if cmDict.get('actions',None) != None:
        cmDict['actions'].update(defaultDict['actions'])
    else:
        cmDict['actions'] = {}
        cmDict['actions'].update(defaultDict['actions'])


    if cmDict.get('provision',None) == None:
        cmDict['provision'] = {}
        cmDict['provision']['storage'] = {}

    for roleName in cmDict['role']:
        if cmDict['cloud']['image_type'] in ["debian", "ubuntu"]:
            os_setup = "os_setup_debian"

        elif cmDict['cloud']['image_type'] in ["centos"]:
            os_setup = "os_setup_centos"
        else:
            LOG.info("Image type not supported")
            sys.exit(0)

        cmDict['role'][roleName]['actions'].insert(0,os_setup)
        cmDict['role'][roleName]['actions'].insert(1,'frieda')
        
        
        # volume & Master data setup.
        if roleName == "master":
            
            if cmDict['application']['input_data'].get('storage',None) != None:
                temp = copy.copy(cmDict['application']['input_data']['storage'])
                cmDict['provision']['storage']['input_data'] = temp
                cmDict['role'][roleName]['storage'] = 'input_data'

                if cmDict['application']['input_data']['storage'].get('size',None):
                     cmDict['role'][roleName]['actions'].append('mkfs')
                cmDict['role'][roleName]['actions'].append('mount')
                
                
             # Add globusOnline action setup on master, if input data need to be transferred thru globus online.
            if cmDict['application']['input_data'].get('ftp') == "globusonline":
                cmDict['role'][roleName]['actions'].insert('globusonline')
                cmDict['role'][roleName]['actions'].insert('go_master_setup')                           
            
            
            cmDict['role'][roleName]['actions'].extend(['master_data_directories'])

            # duplicate call to master_data_placement for data_placement phase in execute_application
            #cmDict['role'][roleName]['actions'].extend(['master_data_placement'])
            
        else:
            # duplicate call to worker_application_source for data_placement phase in execute_application
            #cmDict['role'][roleName]['actions'].extend(['worker_data_directories','worker_application_source'])
            
            cmDict['role'][roleName]['actions'].extend(['worker_data_directories'])
            

            # Add globusOnline action setup on worker, if output data need to be transferred thru globus online.
            if cmDict['application'].get('output_data'):
                if cmDict['application']['output_data'].get('ftp') == "globusonline":
                        cmDict['role']['worker']['actions'].append('globusonline')            
                        cmDict['role']['worker']['actions'].append('go_worker_setup')

    return cmDict


def create_dir(target_dir):

    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    else:
        try:
            shutil.rmtree(target_dir)
            os.makedirs(target_dir)
        except Exception, e:
            LOG.error(str(e))




def collect_logs(log_dir, ssh_key, cmDict):

    """ Moving logs """


    LOG.info("logs are placed in " + log_dir)


    create_dir(log_dir)

    get_logs_cmd = "scp -i %(ssh_key)s -oUserKnownHostsFile=no -oStrictHostKeyChecking=no %(ssh_user)s@%(master_ip)s:/var/log/frieda/* %(results_dir)s/; mv %(cwd)s/*.log %(results_dir)s" % \
                   {'ssh_key': ssh_key, 'master_ip': os.environ['MASTER_IP'], 'results_dir': log_dir,
                    'ssh_user': cmDict['cloud']['ssh_user'],
                    'cwd': os.getcwd()}

    status, output = commands.getstatusoutput(get_logs_cmd)
    if status > 0:
        LOG.info("command '%s' failed  with status:%s output:%s" % (get_logs_cmd, status, output))
        pass




def collect_output(results_dir, ssh_key, cmDict):
    # Moving output

    LOG.info("The output files are stored in " + results_dir)

    create_dir(results_dir)

    for worker in os.environ['WORKER_IPS'].split(","):
        LOG.info("Getting output to localhost from worker: " + str(worker))
        get_outputs_cmd = "scp -i %(ssh_key)s -oUserKnownHostsFile=no -oStrictHostKeyChecking=no %(ssh_user)s@%(worker)s:%(output)s/* %(results_dir)s" % \
                   {'ssh_key': ssh_key, 'worker': worker, 'results_dir': results_dir,
                    'ssh_user': cmDict['cloud']['ssh_user'],
                    'output': os.path.join(cmDict['frieda']['worker_data_dir'],"output")}


        status, output = commands.getstatusoutput(get_outputs_cmd)
        if status > 0:
            LOG.info("command '%s' failed  with status:%s output:%s" % (get_outputs_cmd, status, output))
            pass




def collect_state(state_dir, ssh_key, cmDict):

    create_dir(state_dir)

    try:
        get_state_master_cmd = "scp -i %(ssh_key)s -oUserKnownHostsFile=no -oStrictHostKeyChecking=no %(ssh_user)s@%(master_ip)s:~/*states*  %(results_dir)s" % \
                   {'ssh_key': ssh_key, 'ssh_user': cmDict['cloud']['ssh_user'], 'master_ip': os.environ['MASTER_IP'], 'results_dir': state_dir}

        status, output = commands.getstatusoutput(get_state_master_cmd)
        if status > 0:
            LOG.info("command '%s' failed  with status:%s output:%s" % (get_state_master_cmd, status, output))
            pass

        for worker in os.environ['WORKER_IPS'].split(","):
            LOG.info("Getting State to localhost from worker: " + str(worker))
            get_state_worker_cmd = "scp -i %(ssh_key)s -oUserKnownHostsFile=no -oStrictHostKeyChecking=no %(ssh_user)s@%(worker)s:~/*states* %(results_dir)s" % \
                       {'ssh_key': ssh_key, 'worker': worker, 'ssh_user': cmDict['cloud']['ssh_user'], 'results_dir': state_dir}


            status, output = commands.getstatusoutput(get_state_worker_cmd)
            if status > 0:
                LOG.info("command '%s' failed  with status:%s output:%s" % (get_state_worker_cmd, status, output))
                pass
    except Exception, e:
        LOG.error(str(e))
        

def execute_application(instance_names, config_file, sconfig, log_dir, resource_config, output_dir, state_dir):
    """

    :param instance_names:
    :param config_file:
    :param sconfig:
    :param log_dir:
    :raise:
    """

    start = time.time()
    ssh_key = os.path.expandvars(os.path.expanduser("${HOME}/.ssh/${EC2_KEYPAIR}.pem"))
    
    if not os.path.exists(config_file):
        raise Exception("%s does not exists" % config_file)


    fh = open(config_file, 'r')
    configStr= fh.read() % os.environ
    
    fh.close()
    cmDict=yaml.load(configStr)
    cmDict = addDefaultActions(cmDict, config_file)

    modified_config_file = os.path.join(os.path.dirname(config_file), os.path.basename(config_file)+".tmp")


    fh=open(modified_config_file, 'w')
    yaml.dump(cmDict, fh)
    fh.close()
    
    config_file = modified_config_file
    
    # storage planner
    # Still need to validate storage planner

    if sconfig:
        LOG.info("Initiating Storage planning")
        storage_cm.generate_storage_plan_cm_config(sconfig, resource_config, config_file )
        sys.exit(0)
    else:
        LOG.info("Application requirement for storage planning not provided")



    # Compute & storage provision
    phase = 'provision'
    LOG.info("Cloud monkey initiated compute provision and preparation")
    # TODO: In Frieda Monkey we need a way to remember what roles are assigned to the 
    # instances. We might need to change Monkey to load the environment file before provisioning it
    friedamonkey(config_file, phase, 'provision', instance_names)
    
    ### need to call data provision thru cloud if volume is used.


    # Data Placement

    phase = 'data_placement'
    
    if ('datamanagement' in cmDict['frieda'] and cmDict['frieda']['datamanagement'] == 'datacoordinator'):
      #print "Placing only source file on worker"
      #LOG.info("Skipped Data Placement Phase on Master. Placing application source on worker")
      actions = "master_env_placement"
      LOG.info("Cloud monkey initiated env-file placement on master in data-coordinator mode")
      friedamonkey(config_file, phase, actions, os.environ['MASTER_ID'])
      actions = "worker_application_source"
      friedamonkey(config_file, phase, actions, " ". join(os.environ['WORKER_IDS'].split(",")))
    else:
      actions = "master_data_placement"
      LOG.info("Cloud monkey initiated data placement on master")
      friedamonkey(config_file, phase, actions, os.environ['MASTER_ID'])

      LOG.info("Cloud monkey initiated data placement on worker")
      actions = "worker_application_source"
      friedamonkey(config_file, phase, actions, " ". join(os.environ['WORKER_IDS'].split(",")))


    # Execution
    phase = 'execution'
    
    LOG.info("Cloud monkey initiated Execution")
    if ('datamanagement' in cmDict['frieda'] and cmDict['frieda']['datamanagement'] == 'datacoordinator'):
      actions = "master_execution_datacoordinator"
    else:
      actions = "master_execution"
    
    friedamonkey(config_file, phase, actions, os.environ['MASTER_ID'])

    # cleanup
    phase = "cleanup"
    if cmDict['application'].get('output_data'):
        if cmDict['application']['output_data'].get('ftp',None) == "globusonline":
            actions="go_cleanup"
            friedamonkey(config_file, phase, actions, " ". join(os.environ['WORKER_IDS'].split(",")))



    # TODO: How do we automate collection of the output?

    # Collect output files
    # collect_output(output_dir, ssh_key, cmDict)

    # Collect Logs
    collect_logs(log_dir, ssh_key, cmDict)

    # Collect State of master, controller and workers
    collect_state(state_dir, ssh_key, cmDict)
    
    
    end = time.time()

    #LOG.info("Terminating the Cluster")
    #friedamonkey(config_file, 'stop','stop')
    #LOG.info("Cluster Terminated")


def main(args):
    start = time.time()
    cm_config = args.__dict__['cm_config']
    if args.__dict__['instance_names']:
        instance_names = " ".join(args.__dict__['instance_names'].split(","))
    else:
        instance_names = None
        
    sconfig=args.__dict__['storage_app_config']
    log_dir = args.__dict__['logs']
    resource_config = args.__dict__['resource_config']
    output_dir = args.__dict__['output']
    state_dir = args.__dict__['state']

    LOG.info("FRIEDA Config file: %s instance_names: %s, Storage App Config: %s" % (cm_config, instance_names, sconfig))
    execute_application(instance_names, cm_config, sconfig, log_dir, resource_config, output_dir, state_dir)
    end = time.time()

    LOG.info("FRIEDA life cycle execution Time: %f" % ( end - start))
    LOG.info("\n*****************\n Stopping FRIEDA life cycle for application \n****************\n"  )


if __name__ == "__main__":
    main()

