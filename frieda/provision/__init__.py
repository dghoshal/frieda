import frieda

def main(args):

    # parse arguments
    # args = parse_args()

    # generate updated cloud monkey configuration with storage plan details

    cm_config = args.__dict__['cm_config']
    if args.action == "provision":
        instance_names = " ".join(args.__dict__['instance_names'].split(","))
    else:
        instance_names = None

    frieda.util.integration.friedamonkey(cm_config, args.action, args.action, instance_names )

if __name__ == "__main__":
    main()

