#!/usr/bin/env python
"""
.. module:: frieda
   :platform: Unix, Mac
   :synopsis:  This module contains the entry point for starting frieda


.. moduleauthor:: Val Hendrix <vchendrix@lbl.gov>
"""

import argparse
import commands
import sys
import time
import os
import pdb
import shutil
import logging
import frieda
import yaml
import frieda.util.integration
import frieda.util.staged
import frieda.preparation
import frieda.execute
import frieda.provision
import frieda.state
import frieda.plan

os.environ['FRIEDA_HOME'] = os.path.join(os.path.dirname(frieda.__file__), "..")
#print "FRIEDA_HOME is %s" % os.environ['FRIEDA_HOME']
FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, filename="test_runner.log")
LOG = logging.getLogger(__name__)

#silence parmiko debugging output
logging.getLogger("paramiko").setLevel(logging.WARNING)

def _parse_known_args(parser):
    """
    Use the given parser to parse the known args. Ignores the help command.

    :param parser: the configuration parser
    :type parser: ConfigParser.RawConfigParser
    """
    # Suppress the help.  We want to get the configuration file.
    args = filter(lambda a: a != '-h' and a != '--help', sys.argv)
    # Add something to prevent an error
    if len(args) <= 1:
        args.append("control")
    elif len(args) >= 2:
        args[1] = "control"
    args, rargs = parser.parse_known_args(args[1:])
    return args

def _addAllParser(subparsers):
    parser_worker = subparsers.add_parser('all',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Executes Application using all stages of FRIEDA application
                                                   life cycle management automatically """)


    parser_worker.set_defaults(func=frieda.util.integration.main)
    parser_worker.add_argument('-c','--cm-config', help='Cloud monkey configuration', required=True)
    parser_worker.add_argument('-i','--instance-names', help='Instance names delimited with comma', required=False)
    parser_worker.add_argument('-s','--storage-app-config', help='Application requirements for storage plan', required=False)
    parser_worker.add_argument('-l','--logs', help='Directory in which application logs are placed', required=False, default="logs")
    parser_worker.add_argument('-o','--output', help='Directory in which application output are placed', required=False, default="output")
    parser_worker.add_argument('-t','--state', help='Directory in which application state is placed', required=False, default="state")
    parser_worker.add_argument('-r','--resource-config', help='Resource file which contains details of the instance details', required=False)

def _addStagedParser(subparsers):
    parser_worker = subparsers.add_parser('staged',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Executes Application using different stages of FRIEDA application
                                                   life cycle management automatically """)


    parser_worker.set_defaults(func=frieda.util.staged.main)
    parser_worker.add_argument('-s','--stage', help='Stage to which the execution continues (provision,master,workers,stop,all,scaleup)', required=False, default="stop")    
    parser_worker.add_argument('-c','--cm-config', help='Cloud monkey configuration', required=True)
    parser_worker.add_argument('-i','--instance-names', help='Instance names delimited with comma', required=False)
    parser_worker.add_argument('-a','--storage-app-config', help='Application requirements for storage plan', required=False)
    parser_worker.add_argument('-l','--logs', help='Directory in which application logs are placed', required=False, default="logs")
    parser_worker.add_argument('-o','--output', help='Directory in which application output are placed', required=False, default="output")
    parser_worker.add_argument('-t','--state', help='Directory in which application state is placed', required=False, default="state")
    parser_worker.add_argument('-r','--resource-config', help='Resource file which contains details of the instance details', required=False)

def _addPlanParser(subparsers):
    parser_worker = subparsers.add_parser('plan',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Executes FRIEDA planning phase based on application description """)

    parser_worker.set_defaults(func=frieda.plan.main)
    parser_worker.add_argument("-s", "--spreqfile", dest="spreqfile", help="Application storage plan request", required=True)
    parser_worker.add_argument("-r", "--resourcefile", dest="resourcefile", help="Resource configuration of the infrastructure", required=True)
    parser_worker.add_argument("-c", "--cmconfigfile", dest="cmconfigfile", help=" cloud monkey configuration", required=True)



def _addProvisionParser(subparsers):
    parser_worker = subparsers.add_parser('provision',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Executes FRIEDA provisioning phase based on input plan""")

    parser_worker.set_defaults(func=frieda.provision.main, action="provision")
    parser_worker.add_argument('-c','--cm-config', help='FRIEDA monkey configuration', required=True)
    parser_worker.add_argument('-i','--instance-names', help='Instance names delimited with comma', required=True)



def _addPreparationParser(subparsers):
    parser_worker = subparsers.add_parser('preparation',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" FRIEDA preparation phase executes user defined FRIEDA actions""")

    parser_worker.set_defaults(func=frieda.preparation.main)
    parser_worker.add_argument('-c','--cm-config', help='FRIEDA monkey configuration', required=True)
    parser_worker.add_argument('-a','--actions', help='List of actions separated by comma', required=True)
    parser_worker.add_argument('-i','--machine-id', help='Machine IDs on which action to be applied, seperated by comma', required=True)


def _addExecutionParser(subparsers):
    parser_worker = subparsers.add_parser('execute',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Executes FRIEDA execution phase """)
    frieda.execute.main(parser_worker)

''' 
This function is currently not supported/implemented.
'''
def _addPlacementParser(subparsers):
    parser_worker = subparsers.add_parser('placement',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Executes FRIEDA data placement phase """)


''' 
This function is currently not supported/implemented.
'''
def _addStateParser(subparsers):
    parser_worker = subparsers.add_parser('state',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" FRIEDA state Manager """)

def _addStopParser(subparsers):
    parser_worker = subparsers.add_parser('stop',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Terminates FRIEDA Cluster """)
    parser_worker.set_defaults(func=frieda.provision.main, action="stop")
    parser_worker.add_argument('-c','--cm-config', help='FRIEDA monkey configuration', required=True)


def _addListParser(subparsers):
    parser_worker = subparsers.add_parser('list',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Lists FRIEDA Cluster """)
    parser_worker.set_defaults(func=frieda.provision.main, action="list")
    parser_worker.add_argument('-c','--cm-config', help='FRIEDA monkey configuration', required=True)
    

def main():
    parser = argparse.ArgumentParser(description="",
                                     prog="frieda",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)



    subparsers = parser.add_subparsers()
    _addAllParser(subparsers)
    _addStagedParser(subparsers)
    _addPlanParser(subparsers)
    _addProvisionParser(subparsers)
    _addPreparationParser(subparsers)
    _addPlacementParser(subparsers)
    _addExecutionParser(subparsers)
    _addStateParser(subparsers)
    _addStopParser(subparsers)
    _addListParser(subparsers)

    args = parser.parse_args()
    if len(args.__dict__) == 0:
        print "Parser not yet implemented for this phase"
        sys.exit(0)

    if args.__dict__.get('log_dir', None):
        if not os.path.exists(args.log_dir):
            os.makedirs(args.log_dir, 0664)

    # call the selected functionw with the parsed arguments
    args.func(args)
    
if __name__ == "__main__":
    main()
